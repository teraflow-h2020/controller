#!/bin/bash
# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

python3 -m grpc_tools.protoc -I=./ --python_out=../src/ --grpc_python_out=../src/ *.proto
#requires installation of protoc-gen-uml
export PATH=${HOME}/protoc-gen-uml/target/universal/stage/bin:$PATH
protoc --uml_out=uml/ -I ./ *.proto
cd uml
java -jar plantuml.7997.jar *.puml
rm *.puml


