# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os, logging, signal, sys, time, threading, multiprocessing
from prometheus_client import start_http_server

from common.Settings import get_setting
from opticalcentralizedattackdetector.Config import (
    GRPC_SERVICE_PORT, GRPC_MAX_WORKERS, GRPC_GRACE_PERIOD, LOG_LEVEL, METRICS_PORT,
    MONITORING_INTERVAL)
from opticalcentralizedattackdetector.proto.context_pb2 import (Empty,
    Context,  ContextId,  ContextIdList,  ContextList,
    Service,  ServiceId,  ServiceIdList,  ServiceList
)
from opticalcentralizedattackdetector.service.OpticalCentralizedAttackDetectorService import OpticalCentralizedAttackDetectorService
from opticalcentralizedattackdetector.client.OpticalCentralizedAttackDetectorClient import OpticalCentralizedAttackDetectorClient

terminate = threading.Event()
LOGGER = None

client: OpticalCentralizedAttackDetectorClient = None

def signal_handler(signal, frame): # pylint: disable=redefined-outer-name
    LOGGER.warning('Terminate signal received')
    terminate.set()

def detect_attack(monitoring_interval):
    time.sleep(10)  # wait for the service to start
    LOGGER.info("Starting the attack detection loop")
    client = OpticalCentralizedAttackDetectorClient(address='localhost', port=GRPC_SERVICE_PORT)
    client.connect()
    while True:  # infinite loop that runs until the terminate is set
        if terminate.is_set():  # if terminate is set
            LOGGER.warning("Stopping execution...")
            client.close()
            break  # break the while and stop execution
        client.DetectAttack(Empty())
        # sleep
        LOGGER.debug("Sleeping for {} seconds...".format(monitoring_interval))
        time.sleep(monitoring_interval)

def main():
    global LOGGER # pylint: disable=global-statement

    service_port = get_setting('OPTICALCENTRALIZEDATTACKDETECTORSERVICE_SERVICE_PORT_GRPC', default=GRPC_SERVICE_PORT)
    max_workers  = get_setting('MAX_WORKERS',                                               default=GRPC_MAX_WORKERS )
    grace_period = get_setting('GRACE_PERIOD',                                              default=GRPC_GRACE_PERIOD)
    log_level    = get_setting('LOG_LEVEL',                                                 default=LOG_LEVEL        )
    metrics_port = get_setting('METRICS_PORT',                                              default=METRICS_PORT     )
    monitoring_interval = get_setting('MONITORING_INTERVAL',                                              default=MONITORING_INTERVAL     )

    logging.basicConfig(level=log_level)
    LOGGER = logging.getLogger(__name__)

    signal.signal(signal.SIGINT,  signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    LOGGER.info('Starting...')

    # Start metrics server
    start_http_server(metrics_port)

    # Starting CentralizedCybersecurity service
    grpc_service = OpticalCentralizedAttackDetectorService(
        port=service_port, max_workers=max_workers, grace_period=grace_period)
    grpc_service.start()

    # p = multiprocessing.Process(target=detect_attack, args=(monitoring_interval, ))
    # p.start()
    detect_attack(monitoring_interval)

    # Wait for Ctrl+C or termination signal
    while not terminate.wait(timeout=0.1): pass

    LOGGER.info('Terminating...')
    grpc_service.stop()
    # p.kill()

    LOGGER.info('Bye')
    return 0

if __name__ == '__main__':
    sys.exit(main())
