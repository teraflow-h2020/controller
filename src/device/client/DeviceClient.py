# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import grpc, logging
from common.tools.client.RetryDecorator import retry, delay_exponential
from device.proto.context_pb2 import Device, DeviceConfig, DeviceId, Empty
from device.proto.device_pb2 import MonitoringSettings
from device.proto.device_pb2_grpc import DeviceServiceStub

LOGGER = logging.getLogger(__name__)
MAX_RETRIES = 15
DELAY_FUNCTION = delay_exponential(initial=0.01, increment=2.0, maximum=5.0)
RETRY_DECORATOR = retry(max_retries=MAX_RETRIES, delay_function=DELAY_FUNCTION, prepare_method_name='connect')

class DeviceClient:
    def __init__(self, address, port):
        self.endpoint = '{:s}:{:s}'.format(str(address), str(port))
        LOGGER.debug('Creating channel to {:s}...'.format(str(self.endpoint)))
        self.channel = None
        self.stub = None
        self.connect()
        LOGGER.debug('Channel created')

    def connect(self):
        self.channel = grpc.insecure_channel(self.endpoint)
        self.stub = DeviceServiceStub(self.channel)

    def close(self):
        if(self.channel is not None): self.channel.close()
        self.channel = None
        self.stub = None

    @RETRY_DECORATOR
    def AddDevice(self, request : Device) -> DeviceId:
        LOGGER.debug('AddDevice request: {:s}'.format(str(request)))
        response = self.stub.AddDevice(request)
        LOGGER.debug('AddDevice result: {:s}'.format(str(response)))
        return response

    @RETRY_DECORATOR
    def ConfigureDevice(self, request : Device) -> DeviceId:
        LOGGER.debug('ConfigureDevice request: {:s}'.format(str(request)))
        response = self.stub.ConfigureDevice(request)
        LOGGER.debug('ConfigureDevice result: {:s}'.format(str(response)))
        return response

    @RETRY_DECORATOR
    def DeleteDevice(self, request : DeviceId) -> Empty:
        LOGGER.debug('DeleteDevice request: {:s}'.format(str(request)))
        response = self.stub.DeleteDevice(request)
        LOGGER.debug('DeleteDevice result: {:s}'.format(str(response)))
        return response

    @RETRY_DECORATOR
    def GetInitialConfig(self, request : DeviceId) -> DeviceConfig:
        LOGGER.debug('GetInitialConfig request: {:s}'.format(str(request)))
        response = self.stub.GetInitialConfig(request)
        LOGGER.debug('GetInitialConfig result: {:s}'.format(str(response)))
        return response

    @RETRY_DECORATOR
    def MonitorDeviceKpi(self, request : MonitoringSettings) -> Empty:
        LOGGER.debug('MonitorDeviceKpi request: {:s}'.format(str(request)))
        response = self.stub.MonitorDeviceKpi(request)
        LOGGER.debug('MonitorDeviceKpi result: {:s}'.format(str(response)))
        return response
