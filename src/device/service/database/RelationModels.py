# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
from common.orm.fields.ForeignKeyField import ForeignKeyField
from common.orm.fields.PrimaryKeyField import PrimaryKeyField
from common.orm.model.Model import Model
from .EndPointModel import EndPointMonitorModel
from .KpiModel import KpiModel

LOGGER = logging.getLogger(__name__)

class EndPointMonitorKpiModel(Model): # pylint: disable=abstract-method
    pk = PrimaryKeyField()
    endpoint_monitor_fk = ForeignKeyField(EndPointMonitorModel)
    kpi_fk = ForeignKeyField(KpiModel)
