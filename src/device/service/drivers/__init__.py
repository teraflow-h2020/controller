# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from common.DeviceTypes import DeviceTypeEnum
from ..driver_api.FilterFields import FilterFieldEnum, ORM_DeviceDriverEnum
from .emulated.EmulatedDriver import EmulatedDriver
from .openconfig.OpenConfigDriver import OpenConfigDriver
from .transport_api.TransportApiDriver import TransportApiDriver
from .p4.p4_driver import P4Driver

DRIVERS = [
    (EmulatedDriver, [
        {
            FilterFieldEnum.DEVICE_TYPE: [
                DeviceTypeEnum.EMULATED_OPTICAL_LINE_SYSTEM,
                DeviceTypeEnum.EMULATED_PACKET_ROUTER,
            ],
            FilterFieldEnum.DRIVER     : [
                ORM_DeviceDriverEnum.UNDEFINED,
                ORM_DeviceDriverEnum.OPENCONFIG,
                ORM_DeviceDriverEnum.TRANSPORT_API
            ],
        }
    ]),
    (OpenConfigDriver, [
        {
            FilterFieldEnum.DEVICE_TYPE: DeviceTypeEnum.PACKET_ROUTER,
            FilterFieldEnum.DRIVER     : ORM_DeviceDriverEnum.OPENCONFIG,
        }
    ]),
    (TransportApiDriver, [
        {
            FilterFieldEnum.DEVICE_TYPE: DeviceTypeEnum.OPTICAL_LINE_SYSTEM,
            FilterFieldEnum.DRIVER     : ORM_DeviceDriverEnum.TRANSPORT_API,
        }
    ]),
    (P4Driver, [
        {
            FilterFieldEnum.DEVICE_TYPE: DeviceTypeEnum.P4_SWITCH,
            FilterFieldEnum.DRIVER     : ORM_DeviceDriverEnum.P4,
        }
    ]),
]
