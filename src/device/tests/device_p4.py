# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
P4 device example configuration.
"""

from common.tools.object_factory.ConfigRule import json_config_rule_set
from common.tools.object_factory.Device import json_device_p4_disabled

DEVICE_P4_ID = 0
DEVICE_P4_NAME = 'device:leaf1'
DEVICE_P4_ADDRESS = '127.0.0.1'
DEVICE_P4_PORT = '50101'
DEVICE_P4_VENDOR = 'Open Networking Foundation'
DEVICE_P4_HW_VER = 'BMv2 simple_switch'
DEVICE_P4_SW_VER = 'Stratum'
DEVICE_P4_PIPECONF = 'org.onosproject.pipelines.fabric'
DEVICE_P4_WORKERS = 2
DEVICE_P4_GRACE_PERIOD = 60

DEVICE_P4_UUID = {'device_uuid': {'uuid': DEVICE_P4_NAME}}
DEVICE_P4 = json_device_p4_disabled(DEVICE_P4_UUID)

DEVICE_P4_CONNECT_RULES = [
    json_config_rule_set('_connect/address', DEVICE_P4_ADDRESS),
    json_config_rule_set('_connect/port', DEVICE_P4_PORT),
    json_config_rule_set('_connect/settings', {
        'id'      : int(DEVICE_P4_ID),
        'name'    : DEVICE_P4_NAME,
        'hw-ver'  : DEVICE_P4_HW_VER,
        'sw-ver'  : DEVICE_P4_SW_VER,
        'pipeconf': DEVICE_P4_PIPECONF
    }),
]

DEVICE_P4_CONFIG_RULES = [
    json_config_rule_set('key1', 'value1'),
]
