# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os, grpc, logging, random
from influxdb import InfluxDBClient
from common.rpc_method_wrapper.Decorator import create_metrics, safe_and_metered_rpc_method
from opticalattackmitigator.proto.optical_attack_mitigator_pb2_grpc import (
    AttackMitigatorServicer)
from opticalattackmitigator.proto.optical_attack_mitigator_pb2 import AttackDescription, AttackResponse

LOGGER = logging.getLogger(__name__)

SERVICE_NAME = 'OpticalAttackMitigator'
METHOD_NAMES = ['NotifyAttack']
METRICS = create_metrics(SERVICE_NAME, METHOD_NAMES)


class OpticalAttackMitigatorServiceServicerImpl(AttackMitigatorServicer):

    def __init__(self):
        LOGGER.debug('Creating Servicer...')
        LOGGER.debug('Servicer Created')

    @safe_and_metered_rpc_method(METRICS, LOGGER)
    def NotifyAttack(self, request : AttackDescription, context : grpc.ServicerContext) -> AttackResponse:
        LOGGER.debug(f"NotifyAttack: {request}")
        response: AttackResponse = AttackResponse()
        response.response_strategy_description = 'The AttackMitigator has received the attack description.'
        return response
