# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

WIM_USERNAME = 'admin'
WIM_PASSWORD = 'admin'

# Ref: https://osm.etsi.org/wikipub/index.php/WIM
WIM_MAPPING  = [
    {
        'device-id'           : 'dev-1',            # pop_switch_dpid
        #'device_interface_id' : ??,                # pop_switch_port
        'service_endpoint_id' : 'ep-1',             # wan_service_endpoint_id
        'service_mapping_info': {                   # wan_service_mapping_info, other extra info
            'bearer': {'bearer-reference': 'dev-1:ep-1:10.0.0.1'},
            'site-id': '1',
        },
        #'switch_dpid'         : ??,                # wan_switch_dpid
        #'switch_port'         : ??,                # wan_switch_port
        #'datacenter_id'       : ??,                # vim_account
    },
    {
        'device-id'           : 'dev-2',            # pop_switch_dpid
        #'device_interface_id' : ??,                # pop_switch_port
        'service_endpoint_id' : 'ep-2',             # wan_service_endpoint_id
        'service_mapping_info': {                   # wan_service_mapping_info, other extra info
            'bearer': {'bearer-reference': 'dev-2:ep-2:10.0.0.2'},
            'site-id': '2',
        },
        #'switch_dpid'         : ??,                # wan_switch_dpid
        #'switch_port'         : ??,                # wan_switch_port
        #'datacenter_id'       : ??,                # vim_account
    },
    {
        'device-id'           : 'dev-3',            # pop_switch_dpid
        #'device_interface_id' : ??,                # pop_switch_port
        'service_endpoint_id' : 'ep-3',             # wan_service_endpoint_id
        'service_mapping_info': {                   # wan_service_mapping_info, other extra info
            'bearer': {'bearer-reference': 'dev-3:ep-3:10.0.0.3'},
            'site-id': '3',
        },
        #'switch_dpid'         : ??,                # wan_switch_dpid
        #'switch_port'         : ??,                # wan_switch_port
        #'datacenter_id'       : ??,                # vim_account
    },
]

SERVICE_TYPE = 'ELINE'

SERVICE_CONNECTION_POINTS_1 = [
    {'service_endpoint_id': 'ep-1',
        'service_endpoint_encapsulation_type': 'dot1q',
        'service_endpoint_encapsulation_info': {'vlan': 1234}},
    {'service_endpoint_id': 'ep-2',
        'service_endpoint_encapsulation_type': 'dot1q',
        'service_endpoint_encapsulation_info': {'vlan': 1234}},
]

SERVICE_CONNECTION_POINTS_2 = [
    {'service_endpoint_id': 'ep-3',
        'service_endpoint_encapsulation_type': 'dot1q',
        'service_endpoint_encapsulation_info': {'vlan': 1234}},
]
