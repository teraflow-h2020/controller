# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
from flask import request
from flask.json import jsonify
from flask_restful import Resource
from common.Constants import DEFAULT_CONTEXT_UUID
from common.Settings import get_setting
from context.client.ContextClient import ContextClient
from context.proto.context_pb2 import ServiceId
from service.client.ServiceClient import ServiceClient
from service.proto.context_pb2 import ServiceStatusEnum
from .tools.Authentication import HTTP_AUTH
from .tools.HttpStatusCodes import HTTP_GATEWAYTIMEOUT, HTTP_NOCONTENT, HTTP_OK, HTTP_SERVERERROR

LOGGER = logging.getLogger(__name__)

class L2VPN_Service(Resource):
    def __init__(self) -> None:
        super().__init__()
        self.context_client = ContextClient(
            get_setting('CONTEXTSERVICE_SERVICE_HOST'), get_setting('CONTEXTSERVICE_SERVICE_PORT_GRPC'))
        self.service_client = ServiceClient(
            get_setting('SERVICESERVICE_SERVICE_HOST'), get_setting('SERVICESERVICE_SERVICE_PORT_GRPC'))

    @HTTP_AUTH.login_required
    def get(self, vpn_id : str):
        LOGGER.debug('VPN_Id: {:s}'.format(str(vpn_id)))
        LOGGER.debug('Request: {:s}'.format(str(request)))

        # pylint: disable=no-member
        service_id_request = ServiceId()
        service_id_request.context_id.context_uuid.uuid = DEFAULT_CONTEXT_UUID
        service_id_request.service_uuid.uuid = vpn_id

        try:
            service_reply = self.context_client.GetService(service_id_request)
            if service_reply.service_id != service_id_request: # pylint: disable=no-member
                raise Exception('Service retrieval failed. Wrong Service Id was returned')
            service_ready_status = ServiceStatusEnum.SERVICESTATUS_ACTIVE
            service_status = service_reply.service_status.service_status
            response = jsonify({})
            response.status_code = HTTP_OK if service_status == service_ready_status else HTTP_GATEWAYTIMEOUT
        except Exception as e: # pylint: disable=broad-except
            LOGGER.exception('Something went wrong Retrieving Service {:s}'.format(str(request)))
            response = jsonify({'error': str(e)})
            response.status_code = HTTP_SERVERERROR
        return response

    @HTTP_AUTH.login_required
    def delete(self, vpn_id : str):
        LOGGER.debug('VPN_Id: {:s}'.format(str(vpn_id)))
        LOGGER.debug('Request: {:s}'.format(str(request)))

        # pylint: disable=no-member
        service_id_request = ServiceId()
        service_id_request.context_id.context_uuid.uuid = DEFAULT_CONTEXT_UUID
        service_id_request.service_uuid.uuid = vpn_id

        try:
            self.service_client.DeleteService(service_id_request)
            response = jsonify({})
            response.status_code = HTTP_NOCONTENT
        except Exception as e: # pylint: disable=broad-except
            LOGGER.exception('Something went wrong Deleting Service {:s}'.format(str(request)))
            response = jsonify({'error': str(e)})
            response.status_code = HTTP_SERVERERROR
        return response
