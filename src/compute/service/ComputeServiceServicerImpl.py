# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import grpc, logging
from common.rpc_method_wrapper.Decorator import create_metrics, safe_and_metered_rpc_method
from compute.proto.compute_pb2_grpc import ComputeServiceServicer
from compute.proto.context_pb2 import (
    AuthenticationResult, Empty, Service, ServiceId, ServiceIdList, ServiceStatus, TeraFlowController)

LOGGER = logging.getLogger(__name__)

SERVICE_NAME = 'Compute'
METHOD_NAMES = [
    'CheckCredentials', 'GetConnectivityServiceStatus', 'CreateConnectivityService', 'EditConnectivityService',
    'DeleteConnectivityService', 'GetAllActiveConnectivityServices', 'ClearAllConnectivityServices'
]
METRICS = create_metrics(SERVICE_NAME, METHOD_NAMES)

class ComputeServiceServicerImpl(ComputeServiceServicer):
    def __init__(self):
        LOGGER.info('Creating Servicer...')
        LOGGER.info('Servicer Created')

    @safe_and_metered_rpc_method(METRICS, LOGGER)
    def CheckCredentials(self, request : TeraFlowController, context : grpc.ServicerContext) -> AuthenticationResult:
        LOGGER.warning('NOT IMPLEMENTED')
        return AuthenticationResult()

    @safe_and_metered_rpc_method(METRICS, LOGGER)
    def GetConnectivityServiceStatus(self, request : ServiceId, context : grpc.ServicerContext) -> ServiceStatus:
        LOGGER.warning('NOT IMPLEMENTED')
        return ServiceStatus()

    @safe_and_metered_rpc_method(METRICS, LOGGER)
    def CreateConnectivityService(self, request : Service, context : grpc.ServicerContext) -> ServiceId:
        LOGGER.warning('NOT IMPLEMENTED')
        return ServiceId()

    @safe_and_metered_rpc_method(METRICS, LOGGER)
    def EditConnectivityService(self, request : Service, context : grpc.ServicerContext) -> ServiceId:
        LOGGER.warning('NOT IMPLEMENTED')
        return ServiceId()

    @safe_and_metered_rpc_method(METRICS, LOGGER)
    def DeleteConnectivityService(self, request : Service, context : grpc.ServicerContext) -> Empty:
        LOGGER.warning('NOT IMPLEMENTED')
        return Empty()

    @safe_and_metered_rpc_method(METRICS, LOGGER)
    def GetAllActiveConnectivityServices(self, request : Empty, context : grpc.ServicerContext) -> ServiceIdList:
        LOGGER.warning('NOT IMPLEMENTED')
        return ServiceIdList()

    @safe_and_metered_rpc_method(METRICS, LOGGER)
    def ClearAllConnectivityServices(self, request : Empty, context : grpc.ServicerContext) -> Empty:
        LOGGER.warning('NOT IMPLEMENTED')
        return Empty()
