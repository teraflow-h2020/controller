# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
from werkzeug.security import generate_password_hash

# General settings
LOG_LEVEL = logging.WARNING

# gRPC settings
GRPC_SERVICE_PORT = 9090
GRPC_MAX_WORKERS  = 10
GRPC_GRACE_PERIOD = 60

# REST-API settings
RESTAPI_SERVICE_PORT = 8080
RESTAPI_BASE_URL = '/restconf/data'
RESTAPI_USERS = {   # TODO: implement a database of credentials and permissions
    'admin': generate_password_hash('admin'),
}

# Prometheus settings
METRICS_PORT = 9192
