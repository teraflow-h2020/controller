# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging, signal, sys, threading
from prometheus_client import start_http_server
from common.Settings import get_setting
from common.orm.Database import Database
from common.orm.Factory import get_database_backend
from common.message_broker.Factory import get_messagebroker_backend
from common.message_broker.MessageBroker import MessageBroker
from context.Config import (
    GRPC_SERVICE_PORT, GRPC_MAX_WORKERS, GRPC_GRACE_PERIOD, LOG_LEVEL, POPULATE_FAKE_DATA, RESTAPI_SERVICE_PORT,
    RESTAPI_BASE_URL, METRICS_PORT)
from .grpc_server.ContextService import ContextService
from .rest_server.Resources import RESOURCES
from .rest_server.Server import Server
from .Populate import populate

terminate = threading.Event()
LOGGER = None

def signal_handler(signal, frame): # pylint: disable=redefined-outer-name
    LOGGER.warning('Terminate signal received')
    terminate.set()

def main():
    global LOGGER # pylint: disable=global-statement

    grpc_service_port    = get_setting('CONTEXTSERVICE_SERVICE_PORT_GRPC', default=GRPC_SERVICE_PORT   )
    max_workers          = get_setting('MAX_WORKERS',                      default=GRPC_MAX_WORKERS    )
    grace_period         = get_setting('GRACE_PERIOD',                     default=GRPC_GRACE_PERIOD   )
    log_level            = get_setting('LOG_LEVEL',                        default=LOG_LEVEL           )
    restapi_service_port = get_setting('CONTEXTSERVICE_SERVICE_PORT_HTTP', default=RESTAPI_SERVICE_PORT)
    restapi_base_url     = get_setting('RESTAPI_BASE_URL',                 default=RESTAPI_BASE_URL    )
    metrics_port         = get_setting('METRICS_PORT',                     default=METRICS_PORT        )
    populate_fake_data   = get_setting('POPULATE_FAKE_DATA',               default=POPULATE_FAKE_DATA  )
    if isinstance(populate_fake_data, str): populate_fake_data = (populate_fake_data.upper() in {'T', '1', 'TRUE'})

    logging.basicConfig(level=log_level)
    LOGGER = logging.getLogger(__name__)

    signal.signal(signal.SIGINT,  signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    LOGGER.info('Starting...')

    # Start metrics server
    start_http_server(metrics_port)

    # Get database instance
    database = Database(get_database_backend())

    # Get message broker instance
    messagebroker = MessageBroker(get_messagebroker_backend())

    # Starting context service
    grpc_service = ContextService(
        database, messagebroker, port=grpc_service_port, max_workers=max_workers, grace_period=grace_period)
    grpc_service.start()

    rest_server = Server(port=restapi_service_port, base_url=restapi_base_url)
    for endpoint_name, resource_class, resource_url in RESOURCES:
        rest_server.add_resource(resource_class, resource_url, endpoint=endpoint_name, resource_class_args=(database,))
    rest_server.start()

    if populate_fake_data:
        LOGGER.info('Populating fake data...')
        populate('127.0.0.1', grpc_service_port)
        LOGGER.info('Fake Data populated')

    # Wait for Ctrl+C or termination signal
    while not terminate.wait(timeout=0.1): pass

    LOGGER.info('Terminating...')
    grpc_service.stop()
    rest_server.shutdown()
    rest_server.join()

    LOGGER.info('Bye')
    return 0

if __name__ == '__main__':
    sys.exit(main())
