# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import copy
from context.client.ContextClient import ContextClient
from context.proto.context_pb2 import Connection, Context, Device, Link, Service, Topology
from context.tests.Objects import (
    CONNECTION_R1_R3, CONTEXT, TOPOLOGY, DEVICE_R1, DEVICE_R1_ID, DEVICE_R2, DEVICE_R2_ID, DEVICE_R3, DEVICE_R3_ID,
    LINK_R1_R2, LINK_R1_R2_ID, LINK_R1_R3, LINK_R1_R3_ID, LINK_R2_R3, LINK_R2_R3_ID, SERVICE_R1_R2, SERVICE_R1_R3,
    SERVICE_R2_R3)

def populate(address, port):
    client = ContextClient(address=address, port=port)

    client.SetContext(Context(**CONTEXT))
    client.SetTopology(Topology(**TOPOLOGY))
    client.SetDevice(Device(**DEVICE_R1))
    client.SetDevice(Device(**DEVICE_R2))
    client.SetDevice(Device(**DEVICE_R3))

    client.SetLink(Link(**LINK_R1_R2))
    client.SetLink(Link(**LINK_R1_R3))
    client.SetLink(Link(**LINK_R2_R3))

    TOPOLOGY_WITH_DEVICES_AND_LINKS = copy.deepcopy(TOPOLOGY)
    TOPOLOGY_WITH_DEVICES_AND_LINKS['device_ids'].append(DEVICE_R1_ID)
    TOPOLOGY_WITH_DEVICES_AND_LINKS['device_ids'].append(DEVICE_R2_ID)
    TOPOLOGY_WITH_DEVICES_AND_LINKS['device_ids'].append(DEVICE_R3_ID)
    TOPOLOGY_WITH_DEVICES_AND_LINKS['link_ids'].append(LINK_R1_R2_ID)
    TOPOLOGY_WITH_DEVICES_AND_LINKS['link_ids'].append(LINK_R1_R3_ID)
    TOPOLOGY_WITH_DEVICES_AND_LINKS['link_ids'].append(LINK_R2_R3_ID)
    client.SetTopology(Topology(**TOPOLOGY_WITH_DEVICES_AND_LINKS))

    client.SetService(Service(**SERVICE_R1_R2))
    client.SetService(Service(**SERVICE_R2_R3))

    client.SetService(Service(**SERVICE_R1_R3))
    client.SetConnection(Connection(**CONNECTION_R1_R3))
