# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
from common.orm.fields.ForeignKeyField import ForeignKeyField
from common.orm.fields.PrimaryKeyField import PrimaryKeyField
from common.orm.model.Model import Model
from .ConnectionModel import ConnectionModel
from .DeviceModel import DeviceModel
from .EndPointModel import EndPointModel
from .LinkModel import LinkModel
from .ServiceModel import ServiceModel
from .TopologyModel import TopologyModel

LOGGER = logging.getLogger(__name__)

class ConnectionSubServiceModel(Model): # pylint: disable=abstract-method
    pk = PrimaryKeyField()
    connection_fk = ForeignKeyField(ConnectionModel)
    sub_service_fk = ForeignKeyField(ServiceModel)

class LinkEndPointModel(Model): # pylint: disable=abstract-method
    pk = PrimaryKeyField()
    link_fk = ForeignKeyField(LinkModel)
    endpoint_fk = ForeignKeyField(EndPointModel)

class ServiceEndPointModel(Model): # pylint: disable=abstract-method
    pk = PrimaryKeyField()
    service_fk = ForeignKeyField(ServiceModel)
    endpoint_fk = ForeignKeyField(EndPointModel)

class TopologyDeviceModel(Model): # pylint: disable=abstract-method
    pk = PrimaryKeyField()
    topology_fk = ForeignKeyField(TopologyModel)
    device_fk = ForeignKeyField(DeviceModel)

class TopologyLinkModel(Model): # pylint: disable=abstract-method
    pk = PrimaryKeyField()
    topology_fk = ForeignKeyField(TopologyModel)
    link_fk = ForeignKeyField(LinkModel)
