# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from common.Constants import DEFAULT_CONTEXT_UUID, DEFAULT_TOPOLOGY_UUID
from common.tools.object_factory.ConfigRule import json_config_rule_set
from common.tools.object_factory.Connection import json_connection, json_connection_id
from common.tools.object_factory.Constraint import json_constraint
from common.tools.object_factory.Context import json_context, json_context_id
from common.tools.object_factory.Device import json_device_id, json_device_packetrouter_disabled
from common.tools.object_factory.EndPoint import json_endpoint, json_endpoint_id
from common.tools.object_factory.Link import json_link, json_link_id
from common.tools.object_factory.Service import json_service_id, json_service_l3nm_planned
from common.tools.object_factory.Topology import json_topology, json_topology_id
from context.proto.kpi_sample_types_pb2 import KpiSampleType


# ----- Context --------------------------------------------------------------------------------------------------------
CONTEXT_ID = json_context_id(DEFAULT_CONTEXT_UUID)
CONTEXT    = json_context(DEFAULT_CONTEXT_UUID)


# ----- Topology -------------------------------------------------------------------------------------------------------
TOPOLOGY_ID = json_topology_id(DEFAULT_TOPOLOGY_UUID, context_id=CONTEXT_ID)
TOPOLOGY    = json_topology(DEFAULT_TOPOLOGY_UUID, context_id=CONTEXT_ID)


# ----- KPI Sample Types -----------------------------------------------------------------------------------------------
PACKET_PORT_SAMPLE_TYPES = [
    KpiSampleType.KPISAMPLETYPE_PACKETS_TRANSMITTED,
    KpiSampleType.KPISAMPLETYPE_PACKETS_RECEIVED,
    KpiSampleType.KPISAMPLETYPE_BYTES_TRANSMITTED,
    KpiSampleType.KPISAMPLETYPE_BYTES_RECEIVED,
]


# ----- Device ---------------------------------------------------------------------------------------------------------
DEVICE_R1_UUID  = 'R1'
DEVICE_R1_ID    = json_device_id(DEVICE_R1_UUID)
DEVICE_R1_EPS   = [
    json_endpoint(DEVICE_R1_ID, 'EP2',   '10G', topology_id=TOPOLOGY_ID, kpi_sample_types=PACKET_PORT_SAMPLE_TYPES),
    json_endpoint(DEVICE_R1_ID, 'EP3',   '10G', topology_id=TOPOLOGY_ID, kpi_sample_types=PACKET_PORT_SAMPLE_TYPES),
    json_endpoint(DEVICE_R1_ID, 'EP100', '10G', topology_id=TOPOLOGY_ID, kpi_sample_types=PACKET_PORT_SAMPLE_TYPES),
]
DEVICE_R1_RULES = [
    json_config_rule_set('dev/rsrc1/value', 'value1'),
    json_config_rule_set('dev/rsrc2/value', 'value2'),
    json_config_rule_set('dev/rsrc3/value', 'value3'),
]
DEVICE_R1       = json_device_packetrouter_disabled(
    DEVICE_R1_UUID, endpoints=DEVICE_R1_EPS, config_rules=DEVICE_R1_RULES)


DEVICE_R2_UUID  = 'R2'
DEVICE_R2_ID    = json_device_id(DEVICE_R2_UUID)
DEVICE_R2_EPS   = [
    json_endpoint(DEVICE_R2_ID, 'EP1',   '10G', topology_id=TOPOLOGY_ID, kpi_sample_types=PACKET_PORT_SAMPLE_TYPES),
    json_endpoint(DEVICE_R2_ID, 'EP3',   '10G', topology_id=TOPOLOGY_ID, kpi_sample_types=PACKET_PORT_SAMPLE_TYPES),
    json_endpoint(DEVICE_R2_ID, 'EP100', '10G', topology_id=TOPOLOGY_ID, kpi_sample_types=PACKET_PORT_SAMPLE_TYPES),
]
DEVICE_R2_RULES = [
    json_config_rule_set('dev/rsrc1/value', 'value4'),
    json_config_rule_set('dev/rsrc2/value', 'value5'),
    json_config_rule_set('dev/rsrc3/value', 'value6'),
]
DEVICE_R2       = json_device_packetrouter_disabled(
    DEVICE_R2_UUID, endpoints=DEVICE_R2_EPS, config_rules=DEVICE_R2_RULES)


DEVICE_R3_UUID  = 'R3'
DEVICE_R3_ID    = json_device_id(DEVICE_R3_UUID)
DEVICE_R3_EPS   = [
    json_endpoint(DEVICE_R3_ID, 'EP1',   '10G', topology_id=TOPOLOGY_ID, kpi_sample_types=PACKET_PORT_SAMPLE_TYPES),
    json_endpoint(DEVICE_R3_ID, 'EP2',   '10G', topology_id=TOPOLOGY_ID, kpi_sample_types=PACKET_PORT_SAMPLE_TYPES),
    json_endpoint(DEVICE_R3_ID, 'EP100', '10G', topology_id=TOPOLOGY_ID, kpi_sample_types=PACKET_PORT_SAMPLE_TYPES),
]
DEVICE_R3_RULES = [
    json_config_rule_set('dev/rsrc1/value', 'value4'),
    json_config_rule_set('dev/rsrc2/value', 'value5'),
    json_config_rule_set('dev/rsrc3/value', 'value6'),
]
DEVICE_R3       = json_device_packetrouter_disabled(
    DEVICE_R3_UUID, endpoints=DEVICE_R3_EPS, config_rules=DEVICE_R3_RULES)


# ----- Link -----------------------------------------------------------------------------------------------------------
LINK_R1_R2_UUID  = 'R1/EP2-R2/EP1'
LINK_R1_R2_ID    = json_link_id(LINK_R1_R2_UUID)
LINK_R1_R2_EPIDS = [
    json_endpoint_id(DEVICE_R1_ID, 'EP2', topology_id=TOPOLOGY_ID),
    json_endpoint_id(DEVICE_R2_ID, 'EP1', topology_id=TOPOLOGY_ID),
]
LINK_R1_R2       = json_link(LINK_R1_R2_UUID, LINK_R1_R2_EPIDS)


LINK_R2_R3_UUID  = 'R2/EP3-R3/EP2'
LINK_R2_R3_ID    = json_link_id(LINK_R2_R3_UUID)
LINK_R2_R3_EPIDS = [
    json_endpoint_id(DEVICE_R2_ID, 'EP3', topology_id=TOPOLOGY_ID),
    json_endpoint_id(DEVICE_R3_ID, 'EP2', topology_id=TOPOLOGY_ID),
]
LINK_R2_R3       = json_link(LINK_R2_R3_UUID, LINK_R2_R3_EPIDS)


LINK_R1_R3_UUID  = 'R1/EP3-R3/EP1'
LINK_R1_R3_ID    = json_link_id(LINK_R1_R3_UUID)
LINK_R1_R3_EPIDS = [
    json_endpoint_id(DEVICE_R1_ID, 'EP3', topology_id=TOPOLOGY_ID),
    json_endpoint_id(DEVICE_R3_ID, 'EP1', topology_id=TOPOLOGY_ID),
]
LINK_R1_R3       = json_link(LINK_R1_R3_UUID, LINK_R1_R3_EPIDS)


# ----- Service --------------------------------------------------------------------------------------------------------
SERVICE_R1_R2_UUID  = 'SVC:R1/EP100-R2/EP100'
SERVICE_R1_R2_ID    = json_service_id(SERVICE_R1_R2_UUID, context_id=CONTEXT_ID)
SERVICE_R1_R2_EPIDS = [
    json_endpoint_id(DEVICE_R1_ID, 'EP100', topology_id=TOPOLOGY_ID),
    json_endpoint_id(DEVICE_R2_ID, 'EP100', topology_id=TOPOLOGY_ID),
]
SERVICE_R1_R2_CONST = [
    json_constraint('latency_ms', '15.2'),
    json_constraint('jitter_us',  '1.2'),
]
SERVICE_R1_R2_RULES = [
    json_config_rule_set('svc/rsrc1/value', 'value7'),
    json_config_rule_set('svc/rsrc2/value', 'value8'),
    json_config_rule_set('svc/rsrc3/value', 'value9'),
]
SERVICE_R1_R2       = json_service_l3nm_planned(
    SERVICE_R1_R2_UUID, endpoint_ids=SERVICE_R1_R2_EPIDS, constraints=SERVICE_R1_R2_CONST,
    config_rules=SERVICE_R1_R2_RULES)


SERVICE_R1_R3_UUID  = 'SVC:R1/EP100-R3/EP100'
SERVICE_R1_R3_ID    = json_service_id(SERVICE_R1_R3_UUID, context_id=CONTEXT_ID)
SERVICE_R1_R3_EPIDS = [
    json_endpoint_id(DEVICE_R1_ID, 'EP100', topology_id=TOPOLOGY_ID),
    json_endpoint_id(DEVICE_R3_ID, 'EP100', topology_id=TOPOLOGY_ID),
]
SERVICE_R1_R3_CONST = [
    json_constraint('latency_ms', '5.8'),
    json_constraint('jitter_us',  '0.1'),
]
SERVICE_R1_R3_RULES = [
    json_config_rule_set('svc/rsrc1/value', 'value7'),
    json_config_rule_set('svc/rsrc2/value', 'value8'),
    json_config_rule_set('svc/rsrc3/value', 'value9'),
]
SERVICE_R1_R3       = json_service_l3nm_planned(
    SERVICE_R1_R3_UUID, endpoint_ids=SERVICE_R1_R3_EPIDS, constraints=SERVICE_R1_R3_CONST,
    config_rules=SERVICE_R1_R3_RULES)


SERVICE_R2_R3_UUID  = 'SVC:R2/EP100-R3/EP100'
SERVICE_R2_R3_ID    = json_service_id(SERVICE_R2_R3_UUID, context_id=CONTEXT_ID)
SERVICE_R2_R3_EPIDS = [
    json_endpoint_id(DEVICE_R2_ID, 'EP100', topology_id=TOPOLOGY_ID),
    json_endpoint_id(DEVICE_R3_ID, 'EP100', topology_id=TOPOLOGY_ID),
]
SERVICE_R2_R3_CONST = [
    json_constraint('latency_ms', '23.1'),
    json_constraint('jitter_us',  '3.4'),
]
SERVICE_R2_R3_RULES = [
    json_config_rule_set('svc/rsrc1/value', 'value7'),
    json_config_rule_set('svc/rsrc2/value', 'value8'),
    json_config_rule_set('svc/rsrc3/value', 'value9'),
]
SERVICE_R2_R3       = json_service_l3nm_planned(
    SERVICE_R2_R3_UUID, endpoint_ids=SERVICE_R2_R3_EPIDS, constraints=SERVICE_R2_R3_CONST,
    config_rules=SERVICE_R2_R3_RULES)


# ----- Connection -----------------------------------------------------------------------------------------------------
CONNECTION_R1_R3_UUID   = 'CON:R1/EP100-R3/EP100'
CONNECTION_R1_R3_ID     = json_connection_id(CONNECTION_R1_R3_UUID)
CONNECTION_R1_R3_EPIDS  = [
    json_endpoint_id(DEVICE_R1_ID, 'EP100', topology_id=TOPOLOGY_ID),
    json_endpoint_id(DEVICE_R1_ID, 'EP2',   topology_id=TOPOLOGY_ID),
    json_endpoint_id(DEVICE_R2_ID, 'EP1',   topology_id=TOPOLOGY_ID),
    json_endpoint_id(DEVICE_R2_ID, 'EP3',   topology_id=TOPOLOGY_ID),
    json_endpoint_id(DEVICE_R3_ID, 'EP2',   topology_id=TOPOLOGY_ID),
    json_endpoint_id(DEVICE_R3_ID, 'EP100', topology_id=TOPOLOGY_ID),
]
CONNECTION_R1_R3_SVCIDS = [SERVICE_R1_R2_ID, SERVICE_R2_R3_ID]
CONNECTION_R1_R3        = json_connection(
    CONNECTION_R1_R3_UUID, service_id=SERVICE_R1_R3_ID, path_hops_endpoint_ids=CONNECTION_R1_R3_EPIDS,
    sub_service_ids=CONNECTION_R1_R3_SVCIDS)
