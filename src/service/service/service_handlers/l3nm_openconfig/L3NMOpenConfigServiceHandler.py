# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import anytree, json, logging
from typing import Any, Dict, List, Optional, Tuple, Union
from common.orm.Database import Database
from common.orm.HighLevel import get_object
from common.orm.backend.Tools import key_to_str
from common.type_checkers.Checkers import chk_length, chk_type
from context.client.ContextClient import ContextClient
from device.client.DeviceClient import DeviceClient
from device.proto.context_pb2 import Device
from service.service.database.ConfigModel import ORM_ConfigActionEnum, get_config_rules
from service.service.database.ContextModel import ContextModel
from service.service.database.DeviceModel import DeviceModel
from service.service.database.ServiceModel import ServiceModel
from service.service.service_handler_api._ServiceHandler import _ServiceHandler
from service.service.service_handler_api.AnyTreeTools import TreeNode, delete_subnode, get_subnode, set_subnode_value
from service.service.service_handlers.Tools import config_rule_set, config_rule_delete

LOGGER = logging.getLogger(__name__)

class L3NMOpenConfigServiceHandler(_ServiceHandler):
    def __init__(   # pylint: disable=super-init-not-called
        self, db_service : ServiceModel, database : Database, context_client : ContextClient,
        device_client : DeviceClient, **settings
    ) -> None:
        self.__db_service = db_service
        self.__database = database
        self.__context_client = context_client # pylint: disable=unused-private-member
        self.__device_client = device_client

        self.__db_context : ContextModel = get_object(self.__database, ContextModel, self.__db_service.context_fk)
        str_service_key = key_to_str([self.__db_context.context_uuid, self.__db_service.service_uuid])
        db_config = get_config_rules(self.__database, str_service_key, 'running')
        self.__resolver = anytree.Resolver(pathattr='name')
        self.__config = TreeNode('.')
        for action, resource_key, resource_value in db_config:
            if action == ORM_ConfigActionEnum.SET:
                try:
                    resource_value = json.loads(resource_value)
                except: # pylint: disable=bare-except
                    pass
                set_subnode_value(self.__resolver, self.__config, resource_key, resource_value)
            elif action == ORM_ConfigActionEnum.DELETE:
                delete_subnode(self.__resolver, self.__config, resource_key)

    def SetEndpoint(self, endpoints : List[Tuple[str, str, Optional[str]]]) -> List[Union[bool, Exception]]:
        chk_type('endpoints', endpoints, list)
        if len(endpoints) == 0: return []

        service_uuid              = self.__db_service.service_uuid
        service_short_uuid        = service_uuid.split('-')[-1]
        network_instance_name     = '{:s}-NetInst'.format(service_short_uuid)
        network_interface_desc    = '{:s}-NetIf'.format(service_uuid)
        network_subinterface_desc = '{:s}-NetSubIf'.format(service_uuid)

        settings : TreeNode = get_subnode(self.__resolver, self.__config, 'settings', None)
        if settings is None: raise Exception('Unable to retrieve service settings')
        json_settings : Dict = settings.value
        route_distinguisher = json_settings.get('route_distinguisher', '0:0')    # '60001:801'
        mtu                 = json_settings.get('mtu',                 1450 )    # 1512
        address_families    = json_settings.get('address_families',    []   )    # ['IPV4']

        results = []
        for endpoint in endpoints:
            try:
                chk_type('endpoint', endpoint, (tuple, list))
                chk_length('endpoint', endpoint, min_length=2, max_length=3)
                if len(endpoint) == 2:
                    device_uuid, endpoint_uuid = endpoint
                else:
                    device_uuid, endpoint_uuid, _ = endpoint # ignore topology_uuid by now

                endpoint_settings_uri = 'device[{:s}]/endpoint[{:s}]/settings'.format(device_uuid, endpoint_uuid)
                endpoint_settings : TreeNode = get_subnode(self.__resolver, self.__config, endpoint_settings_uri, None)
                if endpoint_settings is None:
                    raise Exception('Unable to retrieve service settings for endpoint({:s})'.format(
                        str(endpoint_settings_uri)))
                json_endpoint_settings : Dict = endpoint_settings.value
                router_id           = json_endpoint_settings.get('router_id',           '0.0.0.0')  # '10.95.0.10'
                sub_interface_index = json_endpoint_settings.get('sub_interface_index', 0        )  # 1

                db_device : DeviceModel = get_object(self.__database, DeviceModel, device_uuid, raise_if_not_found=True)
                json_device = db_device.dump(include_config_rules=False, include_drivers=True, include_endpoints=True)
                json_device_config : Dict = json_device.setdefault('device_config', {})
                json_device_config_rules : List = json_device_config.setdefault('config_rules', [])
                json_device_config_rules.extend([
                    config_rule_set(
                        '/network_instance[{:s}]'.format(network_instance_name), {
                            'name': network_instance_name, 'description': network_interface_desc, 'type': 'L3VRF',
                            'router_id': router_id, 'route_distinguisher': route_distinguisher,
                            'address_families': address_families,
                    }),
                    config_rule_set(
                        '/interface[{:s}]'.format(endpoint_uuid), {
                            'name': endpoint_uuid, 'description': network_interface_desc, 'mtu': mtu,
                    }),
                    config_rule_set(
                        '/interface[{:s}]/subinterface[{:d}]'.format(endpoint_uuid, sub_interface_index), {
                            'name': endpoint_uuid, 'index': sub_interface_index,
                            'description': network_subinterface_desc, 'mtu': mtu,
                    }),
                    config_rule_set(
                        '/network_instance[{:s}]/interface[{:s}]'.format(network_instance_name, endpoint_uuid), {
                            'name': network_instance_name, 'id': endpoint_uuid,
                    }),
                ])
                self.__device_client.ConfigureDevice(Device(**json_device))
                results.append(True)
            except Exception as e: # pylint: disable=broad-except
                LOGGER.exception('Unable to SetEndpoint({:s})'.format(str(endpoint)))
                results.append(e)

        return results

    def DeleteEndpoint(self, endpoints : List[Tuple[str, str, Optional[str]]]) -> List[Union[bool, Exception]]:
        chk_type('endpoints', endpoints, list)
        if len(endpoints) == 0: return []

        service_uuid              = self.__db_service.service_uuid
        service_short_uuid        = service_uuid.split('-')[-1]
        network_instance_name     = '{:s}-NetInst'.format(service_short_uuid)

        results = []
        for endpoint in endpoints:
            try:
                chk_type('endpoint', endpoint, (tuple, list))
                chk_length('endpoint', endpoint, min_length=2, max_length=3)
                if len(endpoint) == 2:
                    device_uuid, endpoint_uuid = endpoint
                else:
                    device_uuid, endpoint_uuid, _ = endpoint # ignore topology_uuid by now

                endpoint_settings_uri = 'device[{:s}]/endpoint[{:s}]/settings'.format(device_uuid, endpoint_uuid)
                endpoint_settings : TreeNode = get_subnode(self.__resolver, self.__config, endpoint_settings_uri, None)
                if endpoint_settings is None:
                    raise Exception('Unable to retrieve service settings for endpoint({:s})'.format(
                        str(endpoint_settings_uri)))
                json_endpoint_settings : Dict = endpoint_settings.value
                sub_interface_index = json_endpoint_settings.get('sub_interface_index', 0        )  # 1

                db_device : DeviceModel = get_object(self.__database, DeviceModel, device_uuid, raise_if_not_found=True)
                json_device = db_device.dump(include_config_rules=False, include_drivers=True, include_endpoints=True)
                json_device_config : Dict = json_device.setdefault('device_config', {})
                json_device_config_rules : List = json_device_config.setdefault('config_rules', [])
                json_device_config_rules.extend([
                    config_rule_delete(
                        '/network_instance[{:s}]/interface[{:s}]'.format(network_instance_name, endpoint_uuid), {
                            'name': network_instance_name, 'id': endpoint_uuid
                    }),
                    config_rule_delete(
                        '/interface[{:s}]/subinterface[{:d}]'.format(endpoint_uuid, sub_interface_index), {
                            'name': endpoint_uuid, 'index': sub_interface_index,
                    }),
                    config_rule_delete(
                        '/interface[{:s}]'.format(endpoint_uuid), {
                            'name': endpoint_uuid,
                    }),
                    config_rule_delete(
                        '/network_instance[{:s}]'.format(network_instance_name), {
                            'name': network_instance_name
                    }),
                ])
                self.__device_client.ConfigureDevice(Device(**json_device))
                results.append(True)
            except Exception as e: # pylint: disable=broad-except
                LOGGER.exception('Unable to DeleteEndpoint({:s})'.format(str(endpoint)))
                results.append(e)

        return results

    def SetConstraint(self, constraints : List[Tuple[str, Any]]) -> List[Union[bool, Exception]]:
        chk_type('constraints', constraints, list)
        if len(constraints) == 0: return []

        msg = '[SetConstraint] Method not implemented. Constraints({:s}) are being ignored.'
        LOGGER.warning(msg.format(str(constraints)))
        return [True for _ in range(len(constraints))]

    def DeleteConstraint(self, constraints : List[Tuple[str, Any]]) -> List[Union[bool, Exception]]:
        chk_type('constraints', constraints, list)
        if len(constraints) == 0: return []

        msg = '[DeleteConstraint] Method not implemented. Constraints({:s}) are being ignored.'
        LOGGER.warning(msg.format(str(constraints)))
        return [True for _ in range(len(constraints))]

    def SetConfig(self, resources : List[Tuple[str, Any]]) -> List[Union[bool, Exception]]:
        chk_type('resources', resources, list)
        if len(resources) == 0: return []

        results = []
        for resource in resources:
            try:
                resource_key, resource_value = resource
                resource_value = json.loads(resource_value)
                set_subnode_value(self.__resolver, self.__config, resource_key, resource_value)
                results.append(True)
            except Exception as e: # pylint: disable=broad-except
                LOGGER.exception('Unable to SetConfig({:s})'.format(str(resource)))
                results.append(e)

        return results

    def DeleteConfig(self, resources : List[Tuple[str, Any]]) -> List[Union[bool, Exception]]:
        chk_type('resources', resources, list)
        if len(resources) == 0: return []

        results = []
        for resource in resources:
            try:
                resource_key, _ = resource
                delete_subnode(self.__resolver, self.__config, resource_key)
            except Exception as e: # pylint: disable=broad-except
                LOGGER.exception('Unable to DeleteConfig({:s})'.format(str(resource)))
                results.append(e)

        return results
