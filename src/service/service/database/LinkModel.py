# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging, operator
from typing import Dict, List
from common.orm.fields.PrimaryKeyField import PrimaryKeyField
from common.orm.fields.StringField import StringField
from common.orm.model.Model import Model
from common.orm.HighLevel import get_related_objects

LOGGER = logging.getLogger(__name__)

class LinkModel(Model):
    pk = PrimaryKeyField()
    link_uuid = StringField(required=True, allow_empty=False)

    def dump_id(self) -> Dict:
        return {'link_uuid': {'uuid': self.link_uuid}}

    def dump_endpoint_ids(self) -> List[Dict]:
        from .RelationModels import LinkEndPointModel # pylint: disable=import-outside-toplevel
        db_endpoints = get_related_objects(self, LinkEndPointModel, 'endpoint_fk')
        return [db_endpoint.dump_id() for db_endpoint in sorted(db_endpoints, key=operator.attrgetter('pk'))]

    def dump(self) -> Dict:
        return {
            'link_id': self.dump_id(),
            'link_endpoint_ids': self.dump_endpoint_ids(),
        }
