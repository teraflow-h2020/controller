# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import print_function
import logging
from l3_attackmitigator.proto.l3_attackmitigator_pb2 import (
    EmptyMitigator
)
from l3_attackmitigator.proto.l3_attackmitigator_pb2_grpc import (
    L3AttackmitigatorServicer,
)

LOGGER = logging.getLogger(__name__)

class l3_attackmitigatorServiceServicerImpl(L3AttackmitigatorServicer):

    def __init__(self):
        LOGGER.debug("Creating Servicer...")
        self.last_value = -1
        self.last_tag = 0
    
    def SendOutput(self, request, context):
        # SEND CONFIDENCE TO MITIGATION SERVER
        logging.debug("")
        print("Server received mitigation values...", request.confidence)
        last_value = request.confidence
        last_tag = request.tag
        # RETURN OK TO THE CALLER
        return EmptyMitigator(
            message=f"OK, received values: {last_tag} with confidence {last_value}."
        )

    def GetMitigation(self, request, context):
        # GET OR PERFORM MITIGATION STRATEGY
        logging.debug("")
        print("Returing mitigation strategy...")
        k = self.last_value * 2
        return EmptyMitigator(
            message=f"Mitigation with double confidence = {k}"
        )


    
