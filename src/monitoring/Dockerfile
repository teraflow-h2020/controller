# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM python:3-slim

# Install dependencies
RUN apt-get --yes --quiet --quiet update && \
    apt-get --yes --quiet --quiet install wget g++ && \
    rm -rf /var/lib/apt/lists/*

# show python logs as they occur
ENV PYTHONUNBUFFERED=0

# download the grpc health probe
RUN GRPC_HEALTH_PROBE_VERSION=v0.2.0 && \
    wget -qO/bin/grpc_health_probe https://github.com/grpc-ecosystem/grpc-health-probe/releases/download/${GRPC_HEALTH_PROBE_VERSION}/grpc_health_probe-linux-amd64 && \
    chmod +x /bin/grpc_health_probe

# Get generic Python packages
RUN python3 -m pip install --upgrade pip setuptools wheel pip-tools

# Set working directory
WORKDIR /var/teraflow

# Create module sub-folders
RUN mkdir -p /var/teraflow/monitoring
RUN mkdir -p /var/teraflow/common
RUN mkdir -p /var/teraflow/common/tools
RUN mkdir -p /var/teraflow/common/rpc_method_wrapper
RUN mkdir -p /var/teraflow/device
RUN mkdir -p /var/teraflow/device/proto
RUN mkdir -p /var/teraflow/device/client
RUN mkdir -p /var/teraflow/context

# Get Python packages per module
COPY monitoring/requirements.in requirements.in
RUN pip-compile --output-file=requirements.txt requirements.in
RUN python3 -m pip install -r requirements.txt

# add files into working directory
COPY monitoring/. monitoring
COPY device/proto/. device/proto
COPY device/client/. device/client
COPY device/Config.py device
COPY common/. common
COPY context/. context

RUN rm -r common/message_broker/tests
RUN rm -r common/orm/tests
RUN rm -r common/rpc_method_wrapper/tests
RUN rm -r context/tests/test_unitary.py

ENTRYPOINT ["python", "-m", "monitoring.service"]

