# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
from flask import Flask, session
from flask_healthz import healthz, HealthError

from webui.proto.context_pb2 import Empty
from device.client.DeviceClient import DeviceClient
from context.client.ContextClient import ContextClient
from webui.Config import (CONTEXT_SERVICE_ADDRESS, CONTEXT_SERVICE_PORT,
                DEVICE_SERVICE_ADDRESS, DEVICE_SERVICE_PORT)


def get_working_context() -> str:
    if 'context_uuid' in session:
        return session['context_uuid']
    else:
        return 'Not selected'


def liveness():
    pass


def readiness():
    try:  # this component is ready when it is able to connect with the other components it depends on
        context_client: ContextClient = ContextClient(CONTEXT_SERVICE_ADDRESS, CONTEXT_SERVICE_PORT)
        context_client.connect()
        context_client.close()
        device_client: DeviceClient = DeviceClient(DEVICE_SERVICE_ADDRESS, DEVICE_SERVICE_PORT)
        device_client.connect()
        device_client.close()
    except Exception as e:
        raise HealthError('Can\'t connect with the service: ' + e.details())


def create_app(use_config=None):
    app = Flask(__name__)
    if use_config:
        app.config.from_mapping(**use_config)
    
    app.config.update(HEALTHZ={
        'live': liveness,
        'ready': readiness
    })
    
    app.register_blueprint(healthz, url_prefix='/healthz')

    from webui.service.main.routes import main
    app.register_blueprint(main)

    from webui.service.service.routes import service
    app.register_blueprint(service)

    from webui.service.device.routes import device
    app.register_blueprint(device)

    app.jinja_env.globals.update(get_working_context=get_working_context)

    return app
