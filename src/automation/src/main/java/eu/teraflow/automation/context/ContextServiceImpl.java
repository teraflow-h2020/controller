/*
 * Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.teraflow.automation.context;

import eu.teraflow.automation.device.model.Device;
import eu.teraflow.automation.device.model.DeviceEvent;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class ContextServiceImpl implements ContextService {

    private final ContextGateway contextGateway;

    @Inject
    public ContextServiceImpl(ContextGateway contextGateway) {
        this.contextGateway = contextGateway;
    }

    @Override
    public Uni<Device> getDevice(String deviceId) {
        return contextGateway.getDevice(deviceId);
    }

    @Override
    public Multi<DeviceEvent> getDeviceEvents() {
        return contextGateway.getDeviceEvents();
    }
}
