/*
 * Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.teraflow.automation.device.model;

import java.util.List;
import java.util.stream.Collectors;

public class DeviceConfig {

    private final List<ConfigRule> configRules;

    public DeviceConfig(List<ConfigRule> configRules) {

        this.configRules = configRules;
    }

    public List<ConfigRule> getConfigRules() {
        return configRules;
    }

    @Override
    public String toString() {
        final var configRulesDescription =
                configRules.stream().map(ConfigRule::toString).collect(Collectors.joining(", "));
        return String.format("%s[%s]", getClass().getSimpleName(), configRulesDescription);
    }
}
