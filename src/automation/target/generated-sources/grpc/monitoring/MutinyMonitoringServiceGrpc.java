package monitoring;

import static monitoring.MonitoringServiceGrpc.getServiceDescriptor;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;

@javax.annotation.Generated(
value = "by Mutiny Grpc generator",
comments = "Source: monitoring.proto")
public final class MutinyMonitoringServiceGrpc implements io.quarkus.grpc.runtime.MutinyGrpc {
    private MutinyMonitoringServiceGrpc() {}

    public static MutinyMonitoringServiceStub newMutinyStub(io.grpc.Channel channel) {
        return new MutinyMonitoringServiceStub(channel);
    }

    
    public static final class MutinyMonitoringServiceStub extends io.grpc.stub.AbstractStub<MutinyMonitoringServiceStub> implements io.quarkus.grpc.runtime.MutinyStub {
        private MonitoringServiceGrpc.MonitoringServiceStub delegateStub;

        private MutinyMonitoringServiceStub(io.grpc.Channel channel) {
            super(channel);
            delegateStub = MonitoringServiceGrpc.newStub(channel);
        }

        private MutinyMonitoringServiceStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            super(channel, callOptions);
            delegateStub = MonitoringServiceGrpc.newStub(channel).build(channel, callOptions);
        }

        @Override
        protected MutinyMonitoringServiceStub build(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            return new MutinyMonitoringServiceStub(channel, callOptions);
        }

        
        public io.smallrye.mutiny.Uni<monitoring.Monitoring.KpiId> createKpi(monitoring.Monitoring.KpiDescriptor request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::createKpi);
        }

        
        public io.smallrye.mutiny.Uni<monitoring.Monitoring.KpiDescriptor> getKpiDescriptor(monitoring.Monitoring.KpiId request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::getKpiDescriptor);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Empty> includeKpi(monitoring.Monitoring.Kpi request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::includeKpi);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Empty> monitorKpi(monitoring.Monitoring.MonitorKpiRequest request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::monitorKpi);
        }

        
        public io.smallrye.mutiny.Uni<monitoring.Monitoring.Kpi> getInstantKpi(monitoring.Monitoring.KpiId request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::getInstantKpi);
        }

        
        public io.smallrye.mutiny.Multi<monitoring.Monitoring.Kpi> getStreamKpi(monitoring.Monitoring.KpiId request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToMany(request, delegateStub::getStreamKpi);
        }

    }

    
    public static abstract class MonitoringServiceImplBase implements io.grpc.BindableService {

        private String compression;
        /**
        * Set whether the server will try to use a compressed response.
        *
        * @param compression the compression, e.g {@code gzip}
        */
        public MonitoringServiceImplBase withCompression(String compression) {
        this.compression = compression;
        return this;
        }


        
        public io.smallrye.mutiny.Uni<monitoring.Monitoring.KpiId> createKpi(monitoring.Monitoring.KpiDescriptor request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<monitoring.Monitoring.KpiDescriptor> getKpiDescriptor(monitoring.Monitoring.KpiId request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Empty> includeKpi(monitoring.Monitoring.Kpi request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Empty> monitorKpi(monitoring.Monitoring.MonitorKpiRequest request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<monitoring.Monitoring.Kpi> getInstantKpi(monitoring.Monitoring.KpiId request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Multi<monitoring.Monitoring.Kpi> getStreamKpi(monitoring.Monitoring.KpiId request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
            return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
                    .addMethod(
                            monitoring.MonitoringServiceGrpc.getCreateKpiMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            monitoring.Monitoring.KpiDescriptor,
                                            monitoring.Monitoring.KpiId>(
                                            this, METHODID_CREATE_KPI, compression)))
                    .addMethod(
                            monitoring.MonitoringServiceGrpc.getGetKpiDescriptorMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            monitoring.Monitoring.KpiId,
                                            monitoring.Monitoring.KpiDescriptor>(
                                            this, METHODID_GET_KPI_DESCRIPTOR, compression)))
                    .addMethod(
                            monitoring.MonitoringServiceGrpc.getIncludeKpiMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            monitoring.Monitoring.Kpi,
                                            context.ContextOuterClass.Empty>(
                                            this, METHODID_INCLUDE_KPI, compression)))
                    .addMethod(
                            monitoring.MonitoringServiceGrpc.getMonitorKpiMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            monitoring.Monitoring.MonitorKpiRequest,
                                            context.ContextOuterClass.Empty>(
                                            this, METHODID_MONITOR_KPI, compression)))
                    .addMethod(
                            monitoring.MonitoringServiceGrpc.getGetStreamKpiMethod(),
                            asyncServerStreamingCall(
                                    new MethodHandlers<
                                            monitoring.Monitoring.KpiId,
                                            monitoring.Monitoring.Kpi>(
                                            this, METHODID_GET_STREAM_KPI, compression)))
                    .addMethod(
                            monitoring.MonitoringServiceGrpc.getGetInstantKpiMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            monitoring.Monitoring.KpiId,
                                            monitoring.Monitoring.Kpi>(
                                            this, METHODID_GET_INSTANT_KPI, compression)))
                    .build();
        }
    }

    private static final int METHODID_CREATE_KPI = 0;
    private static final int METHODID_GET_KPI_DESCRIPTOR = 1;
    private static final int METHODID_INCLUDE_KPI = 2;
    private static final int METHODID_MONITOR_KPI = 3;
    private static final int METHODID_GET_STREAM_KPI = 4;
    private static final int METHODID_GET_INSTANT_KPI = 5;

    private static final class MethodHandlers<Req, Resp> implements
            io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
            io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
            io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
            io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
        private final MonitoringServiceImplBase serviceImpl;
        private final int methodId;
        private final String compression;

        MethodHandlers(MonitoringServiceImplBase serviceImpl, int methodId, String compression) {
            this.serviceImpl = serviceImpl;
            this.methodId = methodId;
            this.compression = compression;
        }

        @java.lang.Override
        @java.lang.SuppressWarnings("unchecked")
        public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
            switch (methodId) {
                case METHODID_CREATE_KPI:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((monitoring.Monitoring.KpiDescriptor) request,
                            (io.grpc.stub.StreamObserver<monitoring.Monitoring.KpiId>) responseObserver,
                            compression,
                            serviceImpl::createKpi);
                    break;
                case METHODID_GET_KPI_DESCRIPTOR:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((monitoring.Monitoring.KpiId) request,
                            (io.grpc.stub.StreamObserver<monitoring.Monitoring.KpiDescriptor>) responseObserver,
                            compression,
                            serviceImpl::getKpiDescriptor);
                    break;
                case METHODID_INCLUDE_KPI:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((monitoring.Monitoring.Kpi) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty>) responseObserver,
                            compression,
                            serviceImpl::includeKpi);
                    break;
                case METHODID_MONITOR_KPI:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((monitoring.Monitoring.MonitorKpiRequest) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty>) responseObserver,
                            compression,
                            serviceImpl::monitorKpi);
                    break;
                case METHODID_GET_STREAM_KPI:
                    io.quarkus.grpc.runtime.ServerCalls.oneToMany((monitoring.Monitoring.KpiId) request,
                            (io.grpc.stub.StreamObserver<monitoring.Monitoring.Kpi>) responseObserver,
                            compression,
                            serviceImpl::getStreamKpi);
                    break;
                case METHODID_GET_INSTANT_KPI:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((monitoring.Monitoring.KpiId) request,
                            (io.grpc.stub.StreamObserver<monitoring.Monitoring.Kpi>) responseObserver,
                            compression,
                            serviceImpl::getInstantKpi);
                    break;
                default:
                    throw new java.lang.AssertionError();
            }
        }

        @java.lang.Override
        @java.lang.SuppressWarnings("unchecked")
        public io.grpc.stub.StreamObserver<Req> invoke(io.grpc.stub.StreamObserver<Resp> responseObserver) {
            switch (methodId) {
                default:
                    throw new java.lang.AssertionError();
            }
        }
    }

}