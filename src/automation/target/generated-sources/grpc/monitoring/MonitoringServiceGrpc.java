package monitoring;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.38.1)",
    comments = "Source: monitoring.proto")
public final class MonitoringServiceGrpc {

  private MonitoringServiceGrpc() {}

  public static final String SERVICE_NAME = "monitoring.MonitoringService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<monitoring.Monitoring.KpiDescriptor,
      monitoring.Monitoring.KpiId> getCreateKpiMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "CreateKpi",
      requestType = monitoring.Monitoring.KpiDescriptor.class,
      responseType = monitoring.Monitoring.KpiId.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<monitoring.Monitoring.KpiDescriptor,
      monitoring.Monitoring.KpiId> getCreateKpiMethod() {
    io.grpc.MethodDescriptor<monitoring.Monitoring.KpiDescriptor, monitoring.Monitoring.KpiId> getCreateKpiMethod;
    if ((getCreateKpiMethod = MonitoringServiceGrpc.getCreateKpiMethod) == null) {
      synchronized (MonitoringServiceGrpc.class) {
        if ((getCreateKpiMethod = MonitoringServiceGrpc.getCreateKpiMethod) == null) {
          MonitoringServiceGrpc.getCreateKpiMethod = getCreateKpiMethod =
              io.grpc.MethodDescriptor.<monitoring.Monitoring.KpiDescriptor, monitoring.Monitoring.KpiId>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "CreateKpi"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  monitoring.Monitoring.KpiDescriptor.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  monitoring.Monitoring.KpiId.getDefaultInstance()))
              .setSchemaDescriptor(new MonitoringServiceMethodDescriptorSupplier("CreateKpi"))
              .build();
        }
      }
    }
    return getCreateKpiMethod;
  }

  private static volatile io.grpc.MethodDescriptor<monitoring.Monitoring.KpiId,
      monitoring.Monitoring.KpiDescriptor> getGetKpiDescriptorMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetKpiDescriptor",
      requestType = monitoring.Monitoring.KpiId.class,
      responseType = monitoring.Monitoring.KpiDescriptor.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<monitoring.Monitoring.KpiId,
      monitoring.Monitoring.KpiDescriptor> getGetKpiDescriptorMethod() {
    io.grpc.MethodDescriptor<monitoring.Monitoring.KpiId, monitoring.Monitoring.KpiDescriptor> getGetKpiDescriptorMethod;
    if ((getGetKpiDescriptorMethod = MonitoringServiceGrpc.getGetKpiDescriptorMethod) == null) {
      synchronized (MonitoringServiceGrpc.class) {
        if ((getGetKpiDescriptorMethod = MonitoringServiceGrpc.getGetKpiDescriptorMethod) == null) {
          MonitoringServiceGrpc.getGetKpiDescriptorMethod = getGetKpiDescriptorMethod =
              io.grpc.MethodDescriptor.<monitoring.Monitoring.KpiId, monitoring.Monitoring.KpiDescriptor>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetKpiDescriptor"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  monitoring.Monitoring.KpiId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  monitoring.Monitoring.KpiDescriptor.getDefaultInstance()))
              .setSchemaDescriptor(new MonitoringServiceMethodDescriptorSupplier("GetKpiDescriptor"))
              .build();
        }
      }
    }
    return getGetKpiDescriptorMethod;
  }

  private static volatile io.grpc.MethodDescriptor<monitoring.Monitoring.Kpi,
      context.ContextOuterClass.Empty> getIncludeKpiMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "IncludeKpi",
      requestType = monitoring.Monitoring.Kpi.class,
      responseType = context.ContextOuterClass.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<monitoring.Monitoring.Kpi,
      context.ContextOuterClass.Empty> getIncludeKpiMethod() {
    io.grpc.MethodDescriptor<monitoring.Monitoring.Kpi, context.ContextOuterClass.Empty> getIncludeKpiMethod;
    if ((getIncludeKpiMethod = MonitoringServiceGrpc.getIncludeKpiMethod) == null) {
      synchronized (MonitoringServiceGrpc.class) {
        if ((getIncludeKpiMethod = MonitoringServiceGrpc.getIncludeKpiMethod) == null) {
          MonitoringServiceGrpc.getIncludeKpiMethod = getIncludeKpiMethod =
              io.grpc.MethodDescriptor.<monitoring.Monitoring.Kpi, context.ContextOuterClass.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "IncludeKpi"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  monitoring.Monitoring.Kpi.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Empty.getDefaultInstance()))
              .setSchemaDescriptor(new MonitoringServiceMethodDescriptorSupplier("IncludeKpi"))
              .build();
        }
      }
    }
    return getIncludeKpiMethod;
  }

  private static volatile io.grpc.MethodDescriptor<monitoring.Monitoring.MonitorKpiRequest,
      context.ContextOuterClass.Empty> getMonitorKpiMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "MonitorKpi",
      requestType = monitoring.Monitoring.MonitorKpiRequest.class,
      responseType = context.ContextOuterClass.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<monitoring.Monitoring.MonitorKpiRequest,
      context.ContextOuterClass.Empty> getMonitorKpiMethod() {
    io.grpc.MethodDescriptor<monitoring.Monitoring.MonitorKpiRequest, context.ContextOuterClass.Empty> getMonitorKpiMethod;
    if ((getMonitorKpiMethod = MonitoringServiceGrpc.getMonitorKpiMethod) == null) {
      synchronized (MonitoringServiceGrpc.class) {
        if ((getMonitorKpiMethod = MonitoringServiceGrpc.getMonitorKpiMethod) == null) {
          MonitoringServiceGrpc.getMonitorKpiMethod = getMonitorKpiMethod =
              io.grpc.MethodDescriptor.<monitoring.Monitoring.MonitorKpiRequest, context.ContextOuterClass.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "MonitorKpi"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  monitoring.Monitoring.MonitorKpiRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Empty.getDefaultInstance()))
              .setSchemaDescriptor(new MonitoringServiceMethodDescriptorSupplier("MonitorKpi"))
              .build();
        }
      }
    }
    return getMonitorKpiMethod;
  }

  private static volatile io.grpc.MethodDescriptor<monitoring.Monitoring.KpiId,
      monitoring.Monitoring.Kpi> getGetStreamKpiMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetStreamKpi",
      requestType = monitoring.Monitoring.KpiId.class,
      responseType = monitoring.Monitoring.Kpi.class,
      methodType = io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
  public static io.grpc.MethodDescriptor<monitoring.Monitoring.KpiId,
      monitoring.Monitoring.Kpi> getGetStreamKpiMethod() {
    io.grpc.MethodDescriptor<monitoring.Monitoring.KpiId, monitoring.Monitoring.Kpi> getGetStreamKpiMethod;
    if ((getGetStreamKpiMethod = MonitoringServiceGrpc.getGetStreamKpiMethod) == null) {
      synchronized (MonitoringServiceGrpc.class) {
        if ((getGetStreamKpiMethod = MonitoringServiceGrpc.getGetStreamKpiMethod) == null) {
          MonitoringServiceGrpc.getGetStreamKpiMethod = getGetStreamKpiMethod =
              io.grpc.MethodDescriptor.<monitoring.Monitoring.KpiId, monitoring.Monitoring.Kpi>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetStreamKpi"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  monitoring.Monitoring.KpiId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  monitoring.Monitoring.Kpi.getDefaultInstance()))
              .setSchemaDescriptor(new MonitoringServiceMethodDescriptorSupplier("GetStreamKpi"))
              .build();
        }
      }
    }
    return getGetStreamKpiMethod;
  }

  private static volatile io.grpc.MethodDescriptor<monitoring.Monitoring.KpiId,
      monitoring.Monitoring.Kpi> getGetInstantKpiMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetInstantKpi",
      requestType = monitoring.Monitoring.KpiId.class,
      responseType = monitoring.Monitoring.Kpi.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<monitoring.Monitoring.KpiId,
      monitoring.Monitoring.Kpi> getGetInstantKpiMethod() {
    io.grpc.MethodDescriptor<monitoring.Monitoring.KpiId, monitoring.Monitoring.Kpi> getGetInstantKpiMethod;
    if ((getGetInstantKpiMethod = MonitoringServiceGrpc.getGetInstantKpiMethod) == null) {
      synchronized (MonitoringServiceGrpc.class) {
        if ((getGetInstantKpiMethod = MonitoringServiceGrpc.getGetInstantKpiMethod) == null) {
          MonitoringServiceGrpc.getGetInstantKpiMethod = getGetInstantKpiMethod =
              io.grpc.MethodDescriptor.<monitoring.Monitoring.KpiId, monitoring.Monitoring.Kpi>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetInstantKpi"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  monitoring.Monitoring.KpiId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  monitoring.Monitoring.Kpi.getDefaultInstance()))
              .setSchemaDescriptor(new MonitoringServiceMethodDescriptorSupplier("GetInstantKpi"))
              .build();
        }
      }
    }
    return getGetInstantKpiMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static MonitoringServiceStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<MonitoringServiceStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<MonitoringServiceStub>() {
        @java.lang.Override
        public MonitoringServiceStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new MonitoringServiceStub(channel, callOptions);
        }
      };
    return MonitoringServiceStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static MonitoringServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<MonitoringServiceBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<MonitoringServiceBlockingStub>() {
        @java.lang.Override
        public MonitoringServiceBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new MonitoringServiceBlockingStub(channel, callOptions);
        }
      };
    return MonitoringServiceBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static MonitoringServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<MonitoringServiceFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<MonitoringServiceFutureStub>() {
        @java.lang.Override
        public MonitoringServiceFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new MonitoringServiceFutureStub(channel, callOptions);
        }
      };
    return MonitoringServiceFutureStub.newStub(factory, channel);
  }

  /**
   */
  public static abstract class MonitoringServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void createKpi(monitoring.Monitoring.KpiDescriptor request,
        io.grpc.stub.StreamObserver<monitoring.Monitoring.KpiId> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getCreateKpiMethod(), responseObserver);
    }

    /**
     */
    public void getKpiDescriptor(monitoring.Monitoring.KpiId request,
        io.grpc.stub.StreamObserver<monitoring.Monitoring.KpiDescriptor> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetKpiDescriptorMethod(), responseObserver);
    }

    /**
     */
    public void includeKpi(monitoring.Monitoring.Kpi request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getIncludeKpiMethod(), responseObserver);
    }

    /**
     */
    public void monitorKpi(monitoring.Monitoring.MonitorKpiRequest request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getMonitorKpiMethod(), responseObserver);
    }

    /**
     */
    public void getStreamKpi(monitoring.Monitoring.KpiId request,
        io.grpc.stub.StreamObserver<monitoring.Monitoring.Kpi> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetStreamKpiMethod(), responseObserver);
    }

    /**
     */
    public void getInstantKpi(monitoring.Monitoring.KpiId request,
        io.grpc.stub.StreamObserver<monitoring.Monitoring.Kpi> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetInstantKpiMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getCreateKpiMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                monitoring.Monitoring.KpiDescriptor,
                monitoring.Monitoring.KpiId>(
                  this, METHODID_CREATE_KPI)))
          .addMethod(
            getGetKpiDescriptorMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                monitoring.Monitoring.KpiId,
                monitoring.Monitoring.KpiDescriptor>(
                  this, METHODID_GET_KPI_DESCRIPTOR)))
          .addMethod(
            getIncludeKpiMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                monitoring.Monitoring.Kpi,
                context.ContextOuterClass.Empty>(
                  this, METHODID_INCLUDE_KPI)))
          .addMethod(
            getMonitorKpiMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                monitoring.Monitoring.MonitorKpiRequest,
                context.ContextOuterClass.Empty>(
                  this, METHODID_MONITOR_KPI)))
          .addMethod(
            getGetStreamKpiMethod(),
            io.grpc.stub.ServerCalls.asyncServerStreamingCall(
              new MethodHandlers<
                monitoring.Monitoring.KpiId,
                monitoring.Monitoring.Kpi>(
                  this, METHODID_GET_STREAM_KPI)))
          .addMethod(
            getGetInstantKpiMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                monitoring.Monitoring.KpiId,
                monitoring.Monitoring.Kpi>(
                  this, METHODID_GET_INSTANT_KPI)))
          .build();
    }
  }

  /**
   */
  public static final class MonitoringServiceStub extends io.grpc.stub.AbstractAsyncStub<MonitoringServiceStub> {
    private MonitoringServiceStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MonitoringServiceStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new MonitoringServiceStub(channel, callOptions);
    }

    /**
     */
    public void createKpi(monitoring.Monitoring.KpiDescriptor request,
        io.grpc.stub.StreamObserver<monitoring.Monitoring.KpiId> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getCreateKpiMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getKpiDescriptor(monitoring.Monitoring.KpiId request,
        io.grpc.stub.StreamObserver<monitoring.Monitoring.KpiDescriptor> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetKpiDescriptorMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void includeKpi(monitoring.Monitoring.Kpi request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getIncludeKpiMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void monitorKpi(monitoring.Monitoring.MonitorKpiRequest request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getMonitorKpiMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getStreamKpi(monitoring.Monitoring.KpiId request,
        io.grpc.stub.StreamObserver<monitoring.Monitoring.Kpi> responseObserver) {
      io.grpc.stub.ClientCalls.asyncServerStreamingCall(
          getChannel().newCall(getGetStreamKpiMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getInstantKpi(monitoring.Monitoring.KpiId request,
        io.grpc.stub.StreamObserver<monitoring.Monitoring.Kpi> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetInstantKpiMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class MonitoringServiceBlockingStub extends io.grpc.stub.AbstractBlockingStub<MonitoringServiceBlockingStub> {
    private MonitoringServiceBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MonitoringServiceBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new MonitoringServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public monitoring.Monitoring.KpiId createKpi(monitoring.Monitoring.KpiDescriptor request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getCreateKpiMethod(), getCallOptions(), request);
    }

    /**
     */
    public monitoring.Monitoring.KpiDescriptor getKpiDescriptor(monitoring.Monitoring.KpiId request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetKpiDescriptorMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.Empty includeKpi(monitoring.Monitoring.Kpi request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getIncludeKpiMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.Empty monitorKpi(monitoring.Monitoring.MonitorKpiRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getMonitorKpiMethod(), getCallOptions(), request);
    }

    /**
     */
    public java.util.Iterator<monitoring.Monitoring.Kpi> getStreamKpi(
        monitoring.Monitoring.KpiId request) {
      return io.grpc.stub.ClientCalls.blockingServerStreamingCall(
          getChannel(), getGetStreamKpiMethod(), getCallOptions(), request);
    }

    /**
     */
    public monitoring.Monitoring.Kpi getInstantKpi(monitoring.Monitoring.KpiId request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetInstantKpiMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class MonitoringServiceFutureStub extends io.grpc.stub.AbstractFutureStub<MonitoringServiceFutureStub> {
    private MonitoringServiceFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MonitoringServiceFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new MonitoringServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<monitoring.Monitoring.KpiId> createKpi(
        monitoring.Monitoring.KpiDescriptor request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getCreateKpiMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<monitoring.Monitoring.KpiDescriptor> getKpiDescriptor(
        monitoring.Monitoring.KpiId request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetKpiDescriptorMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.Empty> includeKpi(
        monitoring.Monitoring.Kpi request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getIncludeKpiMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.Empty> monitorKpi(
        monitoring.Monitoring.MonitorKpiRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getMonitorKpiMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<monitoring.Monitoring.Kpi> getInstantKpi(
        monitoring.Monitoring.KpiId request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetInstantKpiMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_CREATE_KPI = 0;
  private static final int METHODID_GET_KPI_DESCRIPTOR = 1;
  private static final int METHODID_INCLUDE_KPI = 2;
  private static final int METHODID_MONITOR_KPI = 3;
  private static final int METHODID_GET_STREAM_KPI = 4;
  private static final int METHODID_GET_INSTANT_KPI = 5;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final MonitoringServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(MonitoringServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_CREATE_KPI:
          serviceImpl.createKpi((monitoring.Monitoring.KpiDescriptor) request,
              (io.grpc.stub.StreamObserver<monitoring.Monitoring.KpiId>) responseObserver);
          break;
        case METHODID_GET_KPI_DESCRIPTOR:
          serviceImpl.getKpiDescriptor((monitoring.Monitoring.KpiId) request,
              (io.grpc.stub.StreamObserver<monitoring.Monitoring.KpiDescriptor>) responseObserver);
          break;
        case METHODID_INCLUDE_KPI:
          serviceImpl.includeKpi((monitoring.Monitoring.Kpi) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty>) responseObserver);
          break;
        case METHODID_MONITOR_KPI:
          serviceImpl.monitorKpi((monitoring.Monitoring.MonitorKpiRequest) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty>) responseObserver);
          break;
        case METHODID_GET_STREAM_KPI:
          serviceImpl.getStreamKpi((monitoring.Monitoring.KpiId) request,
              (io.grpc.stub.StreamObserver<monitoring.Monitoring.Kpi>) responseObserver);
          break;
        case METHODID_GET_INSTANT_KPI:
          serviceImpl.getInstantKpi((monitoring.Monitoring.KpiId) request,
              (io.grpc.stub.StreamObserver<monitoring.Monitoring.Kpi>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class MonitoringServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    MonitoringServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return monitoring.Monitoring.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("MonitoringService");
    }
  }

  private static final class MonitoringServiceFileDescriptorSupplier
      extends MonitoringServiceBaseDescriptorSupplier {
    MonitoringServiceFileDescriptorSupplier() {}
  }

  private static final class MonitoringServiceMethodDescriptorSupplier
      extends MonitoringServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    MonitoringServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (MonitoringServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new MonitoringServiceFileDescriptorSupplier())
              .addMethod(getCreateKpiMethod())
              .addMethod(getGetKpiDescriptorMethod())
              .addMethod(getIncludeKpiMethod())
              .addMethod(getMonitorKpiMethod())
              .addMethod(getGetStreamKpiMethod())
              .addMethod(getGetInstantKpiMethod())
              .build();
        }
      }
    }
    return result;
  }
}
