package automation;

import static automation.AutomationServiceGrpc.getServiceDescriptor;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;

@javax.annotation.Generated(
value = "by Mutiny Grpc generator",
comments = "Source: automation.proto")
public final class MutinyAutomationServiceGrpc implements io.quarkus.grpc.runtime.MutinyGrpc {
    private MutinyAutomationServiceGrpc() {}

    public static MutinyAutomationServiceStub newMutinyStub(io.grpc.Channel channel) {
        return new MutinyAutomationServiceStub(channel);
    }

    
    public static final class MutinyAutomationServiceStub extends io.grpc.stub.AbstractStub<MutinyAutomationServiceStub> implements io.quarkus.grpc.runtime.MutinyStub {
        private AutomationServiceGrpc.AutomationServiceStub delegateStub;

        private MutinyAutomationServiceStub(io.grpc.Channel channel) {
            super(channel);
            delegateStub = AutomationServiceGrpc.newStub(channel);
        }

        private MutinyAutomationServiceStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            super(channel, callOptions);
            delegateStub = AutomationServiceGrpc.newStub(channel).build(channel, callOptions);
        }

        @Override
        protected MutinyAutomationServiceStub build(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            return new MutinyAutomationServiceStub(channel, callOptions);
        }

        
        public io.smallrye.mutiny.Uni<automation.Automation.DeviceRole> ztpGetDeviceRole(automation.Automation.DeviceRoleId request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::ztpGetDeviceRole);
        }

        
        public io.smallrye.mutiny.Uni<automation.Automation.DeviceRoleList> ztpGetDeviceRolesByDeviceId(context.ContextOuterClass.DeviceId request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::ztpGetDeviceRolesByDeviceId);
        }

        
        public io.smallrye.mutiny.Uni<automation.Automation.DeviceRoleState> ztpAdd(automation.Automation.DeviceRole request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::ztpAdd);
        }

        
        public io.smallrye.mutiny.Uni<automation.Automation.DeviceRoleState> ztpUpdate(automation.Automation.DeviceRole request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::ztpUpdate);
        }

        
        public io.smallrye.mutiny.Uni<automation.Automation.DeviceRoleState> ztpDelete(automation.Automation.DeviceRole request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::ztpDelete);
        }

        
        public io.smallrye.mutiny.Uni<automation.Automation.DeviceDeletionResult> ztpDeleteAll(automation.Automation.Empty request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::ztpDeleteAll);
        }

    }

    
    public static abstract class AutomationServiceImplBase implements io.grpc.BindableService {

        private String compression;
        /**
        * Set whether the server will try to use a compressed response.
        *
        * @param compression the compression, e.g {@code gzip}
        */
        public AutomationServiceImplBase withCompression(String compression) {
        this.compression = compression;
        return this;
        }


        
        public io.smallrye.mutiny.Uni<automation.Automation.DeviceRole> ztpGetDeviceRole(automation.Automation.DeviceRoleId request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<automation.Automation.DeviceRoleList> ztpGetDeviceRolesByDeviceId(context.ContextOuterClass.DeviceId request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<automation.Automation.DeviceRoleState> ztpAdd(automation.Automation.DeviceRole request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<automation.Automation.DeviceRoleState> ztpUpdate(automation.Automation.DeviceRole request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<automation.Automation.DeviceRoleState> ztpDelete(automation.Automation.DeviceRole request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<automation.Automation.DeviceDeletionResult> ztpDeleteAll(automation.Automation.Empty request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
            return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
                    .addMethod(
                            automation.AutomationServiceGrpc.getZtpGetDeviceRoleMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            automation.Automation.DeviceRoleId,
                                            automation.Automation.DeviceRole>(
                                            this, METHODID_ZTP_GET_DEVICE_ROLE, compression)))
                    .addMethod(
                            automation.AutomationServiceGrpc.getZtpGetDeviceRolesByDeviceIdMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.DeviceId,
                                            automation.Automation.DeviceRoleList>(
                                            this, METHODID_ZTP_GET_DEVICE_ROLES_BY_DEVICE_ID, compression)))
                    .addMethod(
                            automation.AutomationServiceGrpc.getZtpAddMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            automation.Automation.DeviceRole,
                                            automation.Automation.DeviceRoleState>(
                                            this, METHODID_ZTP_ADD, compression)))
                    .addMethod(
                            automation.AutomationServiceGrpc.getZtpUpdateMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            automation.Automation.DeviceRole,
                                            automation.Automation.DeviceRoleState>(
                                            this, METHODID_ZTP_UPDATE, compression)))
                    .addMethod(
                            automation.AutomationServiceGrpc.getZtpDeleteMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            automation.Automation.DeviceRole,
                                            automation.Automation.DeviceRoleState>(
                                            this, METHODID_ZTP_DELETE, compression)))
                    .addMethod(
                            automation.AutomationServiceGrpc.getZtpDeleteAllMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            automation.Automation.Empty,
                                            automation.Automation.DeviceDeletionResult>(
                                            this, METHODID_ZTP_DELETE_ALL, compression)))
                    .build();
        }
    }

    private static final int METHODID_ZTP_GET_DEVICE_ROLE = 0;
    private static final int METHODID_ZTP_GET_DEVICE_ROLES_BY_DEVICE_ID = 1;
    private static final int METHODID_ZTP_ADD = 2;
    private static final int METHODID_ZTP_UPDATE = 3;
    private static final int METHODID_ZTP_DELETE = 4;
    private static final int METHODID_ZTP_DELETE_ALL = 5;

    private static final class MethodHandlers<Req, Resp> implements
            io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
            io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
            io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
            io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
        private final AutomationServiceImplBase serviceImpl;
        private final int methodId;
        private final String compression;

        MethodHandlers(AutomationServiceImplBase serviceImpl, int methodId, String compression) {
            this.serviceImpl = serviceImpl;
            this.methodId = methodId;
            this.compression = compression;
        }

        @java.lang.Override
        @java.lang.SuppressWarnings("unchecked")
        public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
            switch (methodId) {
                case METHODID_ZTP_GET_DEVICE_ROLE:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((automation.Automation.DeviceRoleId) request,
                            (io.grpc.stub.StreamObserver<automation.Automation.DeviceRole>) responseObserver,
                            compression,
                            serviceImpl::ztpGetDeviceRole);
                    break;
                case METHODID_ZTP_GET_DEVICE_ROLES_BY_DEVICE_ID:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.DeviceId) request,
                            (io.grpc.stub.StreamObserver<automation.Automation.DeviceRoleList>) responseObserver,
                            compression,
                            serviceImpl::ztpGetDeviceRolesByDeviceId);
                    break;
                case METHODID_ZTP_ADD:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((automation.Automation.DeviceRole) request,
                            (io.grpc.stub.StreamObserver<automation.Automation.DeviceRoleState>) responseObserver,
                            compression,
                            serviceImpl::ztpAdd);
                    break;
                case METHODID_ZTP_UPDATE:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((automation.Automation.DeviceRole) request,
                            (io.grpc.stub.StreamObserver<automation.Automation.DeviceRoleState>) responseObserver,
                            compression,
                            serviceImpl::ztpUpdate);
                    break;
                case METHODID_ZTP_DELETE:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((automation.Automation.DeviceRole) request,
                            (io.grpc.stub.StreamObserver<automation.Automation.DeviceRoleState>) responseObserver,
                            compression,
                            serviceImpl::ztpDelete);
                    break;
                case METHODID_ZTP_DELETE_ALL:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((automation.Automation.Empty) request,
                            (io.grpc.stub.StreamObserver<automation.Automation.DeviceDeletionResult>) responseObserver,
                            compression,
                            serviceImpl::ztpDeleteAll);
                    break;
                default:
                    throw new java.lang.AssertionError();
            }
        }

        @java.lang.Override
        @java.lang.SuppressWarnings("unchecked")
        public io.grpc.stub.StreamObserver<Req> invoke(io.grpc.stub.StreamObserver<Resp> responseObserver) {
            switch (methodId) {
                default:
                    throw new java.lang.AssertionError();
            }
        }
    }

}