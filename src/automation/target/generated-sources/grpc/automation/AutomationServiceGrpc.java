package automation;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.38.1)",
    comments = "Source: automation.proto")
public final class AutomationServiceGrpc {

  private AutomationServiceGrpc() {}

  public static final String SERVICE_NAME = "automation.AutomationService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<automation.Automation.DeviceRoleId,
      automation.Automation.DeviceRole> getZtpGetDeviceRoleMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ZtpGetDeviceRole",
      requestType = automation.Automation.DeviceRoleId.class,
      responseType = automation.Automation.DeviceRole.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<automation.Automation.DeviceRoleId,
      automation.Automation.DeviceRole> getZtpGetDeviceRoleMethod() {
    io.grpc.MethodDescriptor<automation.Automation.DeviceRoleId, automation.Automation.DeviceRole> getZtpGetDeviceRoleMethod;
    if ((getZtpGetDeviceRoleMethod = AutomationServiceGrpc.getZtpGetDeviceRoleMethod) == null) {
      synchronized (AutomationServiceGrpc.class) {
        if ((getZtpGetDeviceRoleMethod = AutomationServiceGrpc.getZtpGetDeviceRoleMethod) == null) {
          AutomationServiceGrpc.getZtpGetDeviceRoleMethod = getZtpGetDeviceRoleMethod =
              io.grpc.MethodDescriptor.<automation.Automation.DeviceRoleId, automation.Automation.DeviceRole>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ZtpGetDeviceRole"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  automation.Automation.DeviceRoleId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  automation.Automation.DeviceRole.getDefaultInstance()))
              .setSchemaDescriptor(new AutomationServiceMethodDescriptorSupplier("ZtpGetDeviceRole"))
              .build();
        }
      }
    }
    return getZtpGetDeviceRoleMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.DeviceId,
      automation.Automation.DeviceRoleList> getZtpGetDeviceRolesByDeviceIdMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ZtpGetDeviceRolesByDeviceId",
      requestType = context.ContextOuterClass.DeviceId.class,
      responseType = automation.Automation.DeviceRoleList.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.DeviceId,
      automation.Automation.DeviceRoleList> getZtpGetDeviceRolesByDeviceIdMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.DeviceId, automation.Automation.DeviceRoleList> getZtpGetDeviceRolesByDeviceIdMethod;
    if ((getZtpGetDeviceRolesByDeviceIdMethod = AutomationServiceGrpc.getZtpGetDeviceRolesByDeviceIdMethod) == null) {
      synchronized (AutomationServiceGrpc.class) {
        if ((getZtpGetDeviceRolesByDeviceIdMethod = AutomationServiceGrpc.getZtpGetDeviceRolesByDeviceIdMethod) == null) {
          AutomationServiceGrpc.getZtpGetDeviceRolesByDeviceIdMethod = getZtpGetDeviceRolesByDeviceIdMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.DeviceId, automation.Automation.DeviceRoleList>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ZtpGetDeviceRolesByDeviceId"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.DeviceId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  automation.Automation.DeviceRoleList.getDefaultInstance()))
              .setSchemaDescriptor(new AutomationServiceMethodDescriptorSupplier("ZtpGetDeviceRolesByDeviceId"))
              .build();
        }
      }
    }
    return getZtpGetDeviceRolesByDeviceIdMethod;
  }

  private static volatile io.grpc.MethodDescriptor<automation.Automation.DeviceRole,
      automation.Automation.DeviceRoleState> getZtpAddMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ZtpAdd",
      requestType = automation.Automation.DeviceRole.class,
      responseType = automation.Automation.DeviceRoleState.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<automation.Automation.DeviceRole,
      automation.Automation.DeviceRoleState> getZtpAddMethod() {
    io.grpc.MethodDescriptor<automation.Automation.DeviceRole, automation.Automation.DeviceRoleState> getZtpAddMethod;
    if ((getZtpAddMethod = AutomationServiceGrpc.getZtpAddMethod) == null) {
      synchronized (AutomationServiceGrpc.class) {
        if ((getZtpAddMethod = AutomationServiceGrpc.getZtpAddMethod) == null) {
          AutomationServiceGrpc.getZtpAddMethod = getZtpAddMethod =
              io.grpc.MethodDescriptor.<automation.Automation.DeviceRole, automation.Automation.DeviceRoleState>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ZtpAdd"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  automation.Automation.DeviceRole.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  automation.Automation.DeviceRoleState.getDefaultInstance()))
              .setSchemaDescriptor(new AutomationServiceMethodDescriptorSupplier("ZtpAdd"))
              .build();
        }
      }
    }
    return getZtpAddMethod;
  }

  private static volatile io.grpc.MethodDescriptor<automation.Automation.DeviceRole,
      automation.Automation.DeviceRoleState> getZtpUpdateMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ZtpUpdate",
      requestType = automation.Automation.DeviceRole.class,
      responseType = automation.Automation.DeviceRoleState.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<automation.Automation.DeviceRole,
      automation.Automation.DeviceRoleState> getZtpUpdateMethod() {
    io.grpc.MethodDescriptor<automation.Automation.DeviceRole, automation.Automation.DeviceRoleState> getZtpUpdateMethod;
    if ((getZtpUpdateMethod = AutomationServiceGrpc.getZtpUpdateMethod) == null) {
      synchronized (AutomationServiceGrpc.class) {
        if ((getZtpUpdateMethod = AutomationServiceGrpc.getZtpUpdateMethod) == null) {
          AutomationServiceGrpc.getZtpUpdateMethod = getZtpUpdateMethod =
              io.grpc.MethodDescriptor.<automation.Automation.DeviceRole, automation.Automation.DeviceRoleState>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ZtpUpdate"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  automation.Automation.DeviceRole.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  automation.Automation.DeviceRoleState.getDefaultInstance()))
              .setSchemaDescriptor(new AutomationServiceMethodDescriptorSupplier("ZtpUpdate"))
              .build();
        }
      }
    }
    return getZtpUpdateMethod;
  }

  private static volatile io.grpc.MethodDescriptor<automation.Automation.DeviceRole,
      automation.Automation.DeviceRoleState> getZtpDeleteMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ZtpDelete",
      requestType = automation.Automation.DeviceRole.class,
      responseType = automation.Automation.DeviceRoleState.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<automation.Automation.DeviceRole,
      automation.Automation.DeviceRoleState> getZtpDeleteMethod() {
    io.grpc.MethodDescriptor<automation.Automation.DeviceRole, automation.Automation.DeviceRoleState> getZtpDeleteMethod;
    if ((getZtpDeleteMethod = AutomationServiceGrpc.getZtpDeleteMethod) == null) {
      synchronized (AutomationServiceGrpc.class) {
        if ((getZtpDeleteMethod = AutomationServiceGrpc.getZtpDeleteMethod) == null) {
          AutomationServiceGrpc.getZtpDeleteMethod = getZtpDeleteMethod =
              io.grpc.MethodDescriptor.<automation.Automation.DeviceRole, automation.Automation.DeviceRoleState>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ZtpDelete"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  automation.Automation.DeviceRole.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  automation.Automation.DeviceRoleState.getDefaultInstance()))
              .setSchemaDescriptor(new AutomationServiceMethodDescriptorSupplier("ZtpDelete"))
              .build();
        }
      }
    }
    return getZtpDeleteMethod;
  }

  private static volatile io.grpc.MethodDescriptor<automation.Automation.Empty,
      automation.Automation.DeviceDeletionResult> getZtpDeleteAllMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ZtpDeleteAll",
      requestType = automation.Automation.Empty.class,
      responseType = automation.Automation.DeviceDeletionResult.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<automation.Automation.Empty,
      automation.Automation.DeviceDeletionResult> getZtpDeleteAllMethod() {
    io.grpc.MethodDescriptor<automation.Automation.Empty, automation.Automation.DeviceDeletionResult> getZtpDeleteAllMethod;
    if ((getZtpDeleteAllMethod = AutomationServiceGrpc.getZtpDeleteAllMethod) == null) {
      synchronized (AutomationServiceGrpc.class) {
        if ((getZtpDeleteAllMethod = AutomationServiceGrpc.getZtpDeleteAllMethod) == null) {
          AutomationServiceGrpc.getZtpDeleteAllMethod = getZtpDeleteAllMethod =
              io.grpc.MethodDescriptor.<automation.Automation.Empty, automation.Automation.DeviceDeletionResult>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ZtpDeleteAll"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  automation.Automation.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  automation.Automation.DeviceDeletionResult.getDefaultInstance()))
              .setSchemaDescriptor(new AutomationServiceMethodDescriptorSupplier("ZtpDeleteAll"))
              .build();
        }
      }
    }
    return getZtpDeleteAllMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static AutomationServiceStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<AutomationServiceStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<AutomationServiceStub>() {
        @java.lang.Override
        public AutomationServiceStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new AutomationServiceStub(channel, callOptions);
        }
      };
    return AutomationServiceStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static AutomationServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<AutomationServiceBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<AutomationServiceBlockingStub>() {
        @java.lang.Override
        public AutomationServiceBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new AutomationServiceBlockingStub(channel, callOptions);
        }
      };
    return AutomationServiceBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static AutomationServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<AutomationServiceFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<AutomationServiceFutureStub>() {
        @java.lang.Override
        public AutomationServiceFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new AutomationServiceFutureStub(channel, callOptions);
        }
      };
    return AutomationServiceFutureStub.newStub(factory, channel);
  }

  /**
   */
  public static abstract class AutomationServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void ztpGetDeviceRole(automation.Automation.DeviceRoleId request,
        io.grpc.stub.StreamObserver<automation.Automation.DeviceRole> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getZtpGetDeviceRoleMethod(), responseObserver);
    }

    /**
     */
    public void ztpGetDeviceRolesByDeviceId(context.ContextOuterClass.DeviceId request,
        io.grpc.stub.StreamObserver<automation.Automation.DeviceRoleList> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getZtpGetDeviceRolesByDeviceIdMethod(), responseObserver);
    }

    /**
     */
    public void ztpAdd(automation.Automation.DeviceRole request,
        io.grpc.stub.StreamObserver<automation.Automation.DeviceRoleState> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getZtpAddMethod(), responseObserver);
    }

    /**
     */
    public void ztpUpdate(automation.Automation.DeviceRole request,
        io.grpc.stub.StreamObserver<automation.Automation.DeviceRoleState> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getZtpUpdateMethod(), responseObserver);
    }

    /**
     */
    public void ztpDelete(automation.Automation.DeviceRole request,
        io.grpc.stub.StreamObserver<automation.Automation.DeviceRoleState> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getZtpDeleteMethod(), responseObserver);
    }

    /**
     */
    public void ztpDeleteAll(automation.Automation.Empty request,
        io.grpc.stub.StreamObserver<automation.Automation.DeviceDeletionResult> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getZtpDeleteAllMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getZtpGetDeviceRoleMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                automation.Automation.DeviceRoleId,
                automation.Automation.DeviceRole>(
                  this, METHODID_ZTP_GET_DEVICE_ROLE)))
          .addMethod(
            getZtpGetDeviceRolesByDeviceIdMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.DeviceId,
                automation.Automation.DeviceRoleList>(
                  this, METHODID_ZTP_GET_DEVICE_ROLES_BY_DEVICE_ID)))
          .addMethod(
            getZtpAddMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                automation.Automation.DeviceRole,
                automation.Automation.DeviceRoleState>(
                  this, METHODID_ZTP_ADD)))
          .addMethod(
            getZtpUpdateMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                automation.Automation.DeviceRole,
                automation.Automation.DeviceRoleState>(
                  this, METHODID_ZTP_UPDATE)))
          .addMethod(
            getZtpDeleteMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                automation.Automation.DeviceRole,
                automation.Automation.DeviceRoleState>(
                  this, METHODID_ZTP_DELETE)))
          .addMethod(
            getZtpDeleteAllMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                automation.Automation.Empty,
                automation.Automation.DeviceDeletionResult>(
                  this, METHODID_ZTP_DELETE_ALL)))
          .build();
    }
  }

  /**
   */
  public static final class AutomationServiceStub extends io.grpc.stub.AbstractAsyncStub<AutomationServiceStub> {
    private AutomationServiceStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected AutomationServiceStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new AutomationServiceStub(channel, callOptions);
    }

    /**
     */
    public void ztpGetDeviceRole(automation.Automation.DeviceRoleId request,
        io.grpc.stub.StreamObserver<automation.Automation.DeviceRole> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getZtpGetDeviceRoleMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void ztpGetDeviceRolesByDeviceId(context.ContextOuterClass.DeviceId request,
        io.grpc.stub.StreamObserver<automation.Automation.DeviceRoleList> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getZtpGetDeviceRolesByDeviceIdMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void ztpAdd(automation.Automation.DeviceRole request,
        io.grpc.stub.StreamObserver<automation.Automation.DeviceRoleState> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getZtpAddMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void ztpUpdate(automation.Automation.DeviceRole request,
        io.grpc.stub.StreamObserver<automation.Automation.DeviceRoleState> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getZtpUpdateMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void ztpDelete(automation.Automation.DeviceRole request,
        io.grpc.stub.StreamObserver<automation.Automation.DeviceRoleState> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getZtpDeleteMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void ztpDeleteAll(automation.Automation.Empty request,
        io.grpc.stub.StreamObserver<automation.Automation.DeviceDeletionResult> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getZtpDeleteAllMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class AutomationServiceBlockingStub extends io.grpc.stub.AbstractBlockingStub<AutomationServiceBlockingStub> {
    private AutomationServiceBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected AutomationServiceBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new AutomationServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public automation.Automation.DeviceRole ztpGetDeviceRole(automation.Automation.DeviceRoleId request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getZtpGetDeviceRoleMethod(), getCallOptions(), request);
    }

    /**
     */
    public automation.Automation.DeviceRoleList ztpGetDeviceRolesByDeviceId(context.ContextOuterClass.DeviceId request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getZtpGetDeviceRolesByDeviceIdMethod(), getCallOptions(), request);
    }

    /**
     */
    public automation.Automation.DeviceRoleState ztpAdd(automation.Automation.DeviceRole request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getZtpAddMethod(), getCallOptions(), request);
    }

    /**
     */
    public automation.Automation.DeviceRoleState ztpUpdate(automation.Automation.DeviceRole request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getZtpUpdateMethod(), getCallOptions(), request);
    }

    /**
     */
    public automation.Automation.DeviceRoleState ztpDelete(automation.Automation.DeviceRole request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getZtpDeleteMethod(), getCallOptions(), request);
    }

    /**
     */
    public automation.Automation.DeviceDeletionResult ztpDeleteAll(automation.Automation.Empty request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getZtpDeleteAllMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class AutomationServiceFutureStub extends io.grpc.stub.AbstractFutureStub<AutomationServiceFutureStub> {
    private AutomationServiceFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected AutomationServiceFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new AutomationServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<automation.Automation.DeviceRole> ztpGetDeviceRole(
        automation.Automation.DeviceRoleId request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getZtpGetDeviceRoleMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<automation.Automation.DeviceRoleList> ztpGetDeviceRolesByDeviceId(
        context.ContextOuterClass.DeviceId request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getZtpGetDeviceRolesByDeviceIdMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<automation.Automation.DeviceRoleState> ztpAdd(
        automation.Automation.DeviceRole request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getZtpAddMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<automation.Automation.DeviceRoleState> ztpUpdate(
        automation.Automation.DeviceRole request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getZtpUpdateMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<automation.Automation.DeviceRoleState> ztpDelete(
        automation.Automation.DeviceRole request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getZtpDeleteMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<automation.Automation.DeviceDeletionResult> ztpDeleteAll(
        automation.Automation.Empty request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getZtpDeleteAllMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_ZTP_GET_DEVICE_ROLE = 0;
  private static final int METHODID_ZTP_GET_DEVICE_ROLES_BY_DEVICE_ID = 1;
  private static final int METHODID_ZTP_ADD = 2;
  private static final int METHODID_ZTP_UPDATE = 3;
  private static final int METHODID_ZTP_DELETE = 4;
  private static final int METHODID_ZTP_DELETE_ALL = 5;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final AutomationServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(AutomationServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_ZTP_GET_DEVICE_ROLE:
          serviceImpl.ztpGetDeviceRole((automation.Automation.DeviceRoleId) request,
              (io.grpc.stub.StreamObserver<automation.Automation.DeviceRole>) responseObserver);
          break;
        case METHODID_ZTP_GET_DEVICE_ROLES_BY_DEVICE_ID:
          serviceImpl.ztpGetDeviceRolesByDeviceId((context.ContextOuterClass.DeviceId) request,
              (io.grpc.stub.StreamObserver<automation.Automation.DeviceRoleList>) responseObserver);
          break;
        case METHODID_ZTP_ADD:
          serviceImpl.ztpAdd((automation.Automation.DeviceRole) request,
              (io.grpc.stub.StreamObserver<automation.Automation.DeviceRoleState>) responseObserver);
          break;
        case METHODID_ZTP_UPDATE:
          serviceImpl.ztpUpdate((automation.Automation.DeviceRole) request,
              (io.grpc.stub.StreamObserver<automation.Automation.DeviceRoleState>) responseObserver);
          break;
        case METHODID_ZTP_DELETE:
          serviceImpl.ztpDelete((automation.Automation.DeviceRole) request,
              (io.grpc.stub.StreamObserver<automation.Automation.DeviceRoleState>) responseObserver);
          break;
        case METHODID_ZTP_DELETE_ALL:
          serviceImpl.ztpDeleteAll((automation.Automation.Empty) request,
              (io.grpc.stub.StreamObserver<automation.Automation.DeviceDeletionResult>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class AutomationServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    AutomationServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return automation.Automation.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("AutomationService");
    }
  }

  private static final class AutomationServiceFileDescriptorSupplier
      extends AutomationServiceBaseDescriptorSupplier {
    AutomationServiceFileDescriptorSupplier() {}
  }

  private static final class AutomationServiceMethodDescriptorSupplier
      extends AutomationServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    AutomationServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (AutomationServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new AutomationServiceFileDescriptorSupplier())
              .addMethod(getZtpGetDeviceRoleMethod())
              .addMethod(getZtpGetDeviceRolesByDeviceIdMethod())
              .addMethod(getZtpAddMethod())
              .addMethod(getZtpUpdateMethod())
              .addMethod(getZtpDeleteMethod())
              .addMethod(getZtpDeleteAllMethod())
              .build();
        }
      }
    }
    return result;
  }
}
