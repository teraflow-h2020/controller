package automation;

import io.quarkus.grpc.runtime.MutinyService;

@javax.annotation.Generated(
value = "by Mutiny Grpc generator",
comments = "Source: automation.proto")
public interface AutomationService extends MutinyService {

    
    io.smallrye.mutiny.Uni<automation.Automation.DeviceRole> ztpGetDeviceRole(automation.Automation.DeviceRoleId request);
    
    io.smallrye.mutiny.Uni<automation.Automation.DeviceRoleList> ztpGetDeviceRolesByDeviceId(context.ContextOuterClass.DeviceId request);
    
    io.smallrye.mutiny.Uni<automation.Automation.DeviceRoleState> ztpAdd(automation.Automation.DeviceRole request);
    
    io.smallrye.mutiny.Uni<automation.Automation.DeviceRoleState> ztpUpdate(automation.Automation.DeviceRole request);
    
    io.smallrye.mutiny.Uni<automation.Automation.DeviceRoleState> ztpDelete(automation.Automation.DeviceRole request);
    
    io.smallrye.mutiny.Uni<automation.Automation.DeviceDeletionResult> ztpDeleteAll(automation.Automation.Empty request);
    
    
    

}