package automation;

import io.grpc.BindableService;
import io.quarkus.grpc.GrpcService;
import io.quarkus.grpc.runtime.MutinyBean;

@javax.annotation.Generated(
value = "by Mutiny Grpc generator",
comments = "Source: automation.proto")
public class AutomationServiceBean extends MutinyAutomationServiceGrpc.AutomationServiceImplBase implements BindableService, MutinyBean {

    private final AutomationService delegate;

    AutomationServiceBean(@GrpcService AutomationService delegate) {
       this.delegate = delegate;
    }

    @Override
    public io.smallrye.mutiny.Uni<automation.Automation.DeviceRole> ztpGetDeviceRole(automation.Automation.DeviceRoleId request) {
       try {
         return delegate.ztpGetDeviceRole(request);
       } catch (UnsupportedOperationException e) {
          throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
       }
    }
    @Override
    public io.smallrye.mutiny.Uni<automation.Automation.DeviceRoleList> ztpGetDeviceRolesByDeviceId(context.ContextOuterClass.DeviceId request) {
       try {
         return delegate.ztpGetDeviceRolesByDeviceId(request);
       } catch (UnsupportedOperationException e) {
          throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
       }
    }
    @Override
    public io.smallrye.mutiny.Uni<automation.Automation.DeviceRoleState> ztpAdd(automation.Automation.DeviceRole request) {
       try {
         return delegate.ztpAdd(request);
       } catch (UnsupportedOperationException e) {
          throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
       }
    }
    @Override
    public io.smallrye.mutiny.Uni<automation.Automation.DeviceRoleState> ztpUpdate(automation.Automation.DeviceRole request) {
       try {
         return delegate.ztpUpdate(request);
       } catch (UnsupportedOperationException e) {
          throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
       }
    }
    @Override
    public io.smallrye.mutiny.Uni<automation.Automation.DeviceRoleState> ztpDelete(automation.Automation.DeviceRole request) {
       try {
         return delegate.ztpDelete(request);
       } catch (UnsupportedOperationException e) {
          throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
       }
    }
    @Override
    public io.smallrye.mutiny.Uni<automation.Automation.DeviceDeletionResult> ztpDeleteAll(automation.Automation.Empty request) {
       try {
         return delegate.ztpDeleteAll(request);
       } catch (UnsupportedOperationException e) {
          throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
       }
    }

}