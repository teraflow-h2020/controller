package automation;

import java.util.function.BiFunction;

import io.quarkus.grpc.runtime.MutinyClient;

@javax.annotation.Generated(
value = "by Mutiny Grpc generator",
comments = "Source: automation.proto")
public class AutomationServiceClient implements AutomationService, MutinyClient<MutinyAutomationServiceGrpc.MutinyAutomationServiceStub> {

    private final MutinyAutomationServiceGrpc.MutinyAutomationServiceStub stub;

    public AutomationServiceClient(String name, io.grpc.Channel channel, BiFunction<String, MutinyAutomationServiceGrpc.MutinyAutomationServiceStub, MutinyAutomationServiceGrpc.MutinyAutomationServiceStub> stubConfigurator) {
       this.stub = stubConfigurator.apply(name,MutinyAutomationServiceGrpc.newMutinyStub(channel));
    }

    @Override
    public MutinyAutomationServiceGrpc.MutinyAutomationServiceStub getStub() {
       return stub;
    }

    @Override
    public io.smallrye.mutiny.Uni<automation.Automation.DeviceRole> ztpGetDeviceRole(automation.Automation.DeviceRoleId request) {
       return stub.ztpGetDeviceRole(request);
    }
    @Override
    public io.smallrye.mutiny.Uni<automation.Automation.DeviceRoleList> ztpGetDeviceRolesByDeviceId(context.ContextOuterClass.DeviceId request) {
       return stub.ztpGetDeviceRolesByDeviceId(request);
    }
    @Override
    public io.smallrye.mutiny.Uni<automation.Automation.DeviceRoleState> ztpAdd(automation.Automation.DeviceRole request) {
       return stub.ztpAdd(request);
    }
    @Override
    public io.smallrye.mutiny.Uni<automation.Automation.DeviceRoleState> ztpUpdate(automation.Automation.DeviceRole request) {
       return stub.ztpUpdate(request);
    }
    @Override
    public io.smallrye.mutiny.Uni<automation.Automation.DeviceRoleState> ztpDelete(automation.Automation.DeviceRole request) {
       return stub.ztpDelete(request);
    }
    @Override
    public io.smallrye.mutiny.Uni<automation.Automation.DeviceDeletionResult> ztpDeleteAll(automation.Automation.Empty request) {
       return stub.ztpDeleteAll(request);
    }

}