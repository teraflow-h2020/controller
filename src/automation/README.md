# Automation TeraFlow OS service 

The Automation service, also known as Zero-Touch Provisioning (ZTP), is tested on Ubuntu 20.04. Follow the instructions below to build, test, and run this service on your local environment.

## Automation Teraflow OS service architecture

| The Automation Teraflow OS service architecture consists of six (6) interfaces listed below:                                                                                                                                                 | 
|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1. The `AutomationGateway` interface that implements all the rpc functions that are described in `automation.proto` file.                                                                                                                    | 
| 2. The `ContextGateway` interface that communicates with a `Context` Service gRPC client and implements all the rpc functions that are described in `context.proto` file.                                                                    |
| 3. The `DeviceGateway` interface that communicates with a `Device` Service gRPC client and implements all the rpc functions that are described in `device.proto` file.                                                                       |
| 4. The `AutomationService` interface that implements the `addDevice()` method by communicating with a `Context` gRPC client & a `Device` gRPC client through the use of `ContextService` interface & `DeviceService` interface respectively. |
| 5. The `ContextService` interface that implements the `getDevice()` & `getDeviceEvents()` methods by communicating with a `Context` gRPC client through the use of `ContextGateway` interface.                                               |
| 6. The `DeviceService` interface that implements the `getInitialConfiguration()` & `configureDevice()` methods by communicating with a `Device` gRPC client through the use of `DeviceGateway` interface.                                    |


## Run with dev profile

```bash
./mvnw clean quarkus:dev
```

## Running tests

Run unit and functional tests `./mvnw clean test`

## Deploying on a Kubernetes cluster

To create the K8s manifest file under `target/kubernetes/kubernetes.yml` to be used run

```bash
./mvnw clean package -DskipUTs -DskipITs
``` 

To deploy the application in a K8s cluster run

```bash
kubectl apply -f "manifests/automationservice.yaml"
```
