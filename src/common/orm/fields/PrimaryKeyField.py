# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import annotations
from typing import TYPE_CHECKING
from .StringField import StringField

if TYPE_CHECKING:
    from ..model.Model import Model

class PrimaryKeyField(StringField):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, required=True, allow_empty=False, min_length=1, **kwargs)

    def __set__(self, instance : 'Model', value : str) -> None:
        if (self.name in instance.__dict__) and (instance.__dict__[self.name] is not None):
            raise ValueError('PrimaryKeyField cannot be modified')
        super().__set__(instance, self.validate(value))
