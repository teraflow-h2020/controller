# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import annotations
from typing import Union
from common.type_checkers.Checkers import chk_boolean
from .Field import Field

BOOL_TRUE_VALUES = {'TRUE', 'T', '1'}

class BooleanField(Field):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, type_=bool, **kwargs)

    def validate(self, value : Union[bool, str], try_convert_type=False) -> bool:
        value = self.is_required(value)
        if value is None: return None
        if try_convert_type and isinstance(value, str):
            return value.upper() in BOOL_TRUE_VALUES
        return chk_boolean(self.name, value)
