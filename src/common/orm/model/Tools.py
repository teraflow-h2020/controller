# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from collections import OrderedDict

class NoDupOrderedDict(OrderedDict):
    def __setitem__(self, key, value):
        if key in self: raise NameError('{:s} already defined'.format(str(key)))
        super().__setitem__(key, value)

def format_key(key_pattern, instance, **kwargs):
    attributes = {}
    for attribute_name in instance.__dir__():
        if attribute_name[0] == '_': continue
        attribute_obj = getattr(instance, attribute_name, None)
        if attribute_obj is None: continue
        if type(attribute_obj).__name__ == 'method': continue
        attributes[attribute_name] = attribute_obj
    attributes.update(kwargs)
    return key_pattern.format(**attributes)
