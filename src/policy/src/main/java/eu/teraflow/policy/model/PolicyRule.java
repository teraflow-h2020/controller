/*
 * Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.teraflow.policy.model;

import eu.teraflow.policy.context.model.ContextId;
import eu.teraflow.policy.context.model.ServiceId;

public class PolicyRule {

    private final PolicyRuleId policyRuleId;
    private final PolicyRuleType policyRuleType;
    private final PolicyRulePriority policyRulePriority;
    private final PolicyRuleEvent policyRuleEvent;
    private final PolicyRuleCondition policyRuleCondition;
    private final PolicyRuleAction policyRuleAction;
    private final ServiceId serviceId;
    private final ContextId contextId;

    public PolicyRule(
            PolicyRuleId policyRuleId,
            PolicyRuleType policyRuleType,
            PolicyRulePriority policyRulePriority,
            PolicyRuleEvent policyRuleEvent,
            PolicyRuleCondition policyRuleCondition,
            PolicyRuleAction policyRuleAction,
            ServiceId serviceId,
            ContextId contextId) {
        this.policyRuleId = policyRuleId;
        this.policyRuleType = policyRuleType;
        this.policyRulePriority = policyRulePriority;
        this.policyRuleEvent = policyRuleEvent;
        this.policyRuleCondition = policyRuleCondition;
        this.policyRuleAction = policyRuleAction;
        this.serviceId = serviceId;
        this.contextId = contextId;
    }

    public PolicyRuleId getPolicyRuleId() {
        return policyRuleId;
    }

    public PolicyRuleType getPolicyRuleType() {
        return policyRuleType;
    }

    public PolicyRulePriority getPolicyRulePriority() {
        return policyRulePriority;
    }

    public PolicyRuleEvent getPolicyRuleEvent() {
        return policyRuleEvent;
    }

    public PolicyRuleCondition getPolicyRuleCondition() {
        return policyRuleCondition;
    }

    public PolicyRuleAction getPolicyRuleAction() {
        return policyRuleAction;
    }

    public ServiceId getServiceId() {
        return serviceId;
    }

    public ContextId getContextId() {
        return contextId;
    }
}
