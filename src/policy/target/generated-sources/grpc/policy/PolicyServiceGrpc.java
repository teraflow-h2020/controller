package policy;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.38.1)",
    comments = "Source: policy.proto")
public final class PolicyServiceGrpc {

  private PolicyServiceGrpc() {}

  public static final String SERVICE_NAME = "policy.PolicyService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<policy.Policy.PolicyRule,
      policy.Policy.PolicyRuleState> getPolicyAddMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "PolicyAdd",
      requestType = policy.Policy.PolicyRule.class,
      responseType = policy.Policy.PolicyRuleState.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<policy.Policy.PolicyRule,
      policy.Policy.PolicyRuleState> getPolicyAddMethod() {
    io.grpc.MethodDescriptor<policy.Policy.PolicyRule, policy.Policy.PolicyRuleState> getPolicyAddMethod;
    if ((getPolicyAddMethod = PolicyServiceGrpc.getPolicyAddMethod) == null) {
      synchronized (PolicyServiceGrpc.class) {
        if ((getPolicyAddMethod = PolicyServiceGrpc.getPolicyAddMethod) == null) {
          PolicyServiceGrpc.getPolicyAddMethod = getPolicyAddMethod =
              io.grpc.MethodDescriptor.<policy.Policy.PolicyRule, policy.Policy.PolicyRuleState>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "PolicyAdd"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  policy.Policy.PolicyRule.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  policy.Policy.PolicyRuleState.getDefaultInstance()))
              .setSchemaDescriptor(new PolicyServiceMethodDescriptorSupplier("PolicyAdd"))
              .build();
        }
      }
    }
    return getPolicyAddMethod;
  }

  private static volatile io.grpc.MethodDescriptor<policy.Policy.PolicyRule,
      policy.Policy.PolicyRuleState> getPolicyUpdateMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "PolicyUpdate",
      requestType = policy.Policy.PolicyRule.class,
      responseType = policy.Policy.PolicyRuleState.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<policy.Policy.PolicyRule,
      policy.Policy.PolicyRuleState> getPolicyUpdateMethod() {
    io.grpc.MethodDescriptor<policy.Policy.PolicyRule, policy.Policy.PolicyRuleState> getPolicyUpdateMethod;
    if ((getPolicyUpdateMethod = PolicyServiceGrpc.getPolicyUpdateMethod) == null) {
      synchronized (PolicyServiceGrpc.class) {
        if ((getPolicyUpdateMethod = PolicyServiceGrpc.getPolicyUpdateMethod) == null) {
          PolicyServiceGrpc.getPolicyUpdateMethod = getPolicyUpdateMethod =
              io.grpc.MethodDescriptor.<policy.Policy.PolicyRule, policy.Policy.PolicyRuleState>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "PolicyUpdate"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  policy.Policy.PolicyRule.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  policy.Policy.PolicyRuleState.getDefaultInstance()))
              .setSchemaDescriptor(new PolicyServiceMethodDescriptorSupplier("PolicyUpdate"))
              .build();
        }
      }
    }
    return getPolicyUpdateMethod;
  }

  private static volatile io.grpc.MethodDescriptor<policy.Policy.PolicyRule,
      policy.Policy.PolicyRuleState> getPolicyDeleteMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "PolicyDelete",
      requestType = policy.Policy.PolicyRule.class,
      responseType = policy.Policy.PolicyRuleState.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<policy.Policy.PolicyRule,
      policy.Policy.PolicyRuleState> getPolicyDeleteMethod() {
    io.grpc.MethodDescriptor<policy.Policy.PolicyRule, policy.Policy.PolicyRuleState> getPolicyDeleteMethod;
    if ((getPolicyDeleteMethod = PolicyServiceGrpc.getPolicyDeleteMethod) == null) {
      synchronized (PolicyServiceGrpc.class) {
        if ((getPolicyDeleteMethod = PolicyServiceGrpc.getPolicyDeleteMethod) == null) {
          PolicyServiceGrpc.getPolicyDeleteMethod = getPolicyDeleteMethod =
              io.grpc.MethodDescriptor.<policy.Policy.PolicyRule, policy.Policy.PolicyRuleState>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "PolicyDelete"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  policy.Policy.PolicyRule.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  policy.Policy.PolicyRuleState.getDefaultInstance()))
              .setSchemaDescriptor(new PolicyServiceMethodDescriptorSupplier("PolicyDelete"))
              .build();
        }
      }
    }
    return getPolicyDeleteMethod;
  }

  private static volatile io.grpc.MethodDescriptor<policy.Policy.PolicyRuleId,
      policy.Policy.PolicyRule> getGetPolicyMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetPolicy",
      requestType = policy.Policy.PolicyRuleId.class,
      responseType = policy.Policy.PolicyRule.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<policy.Policy.PolicyRuleId,
      policy.Policy.PolicyRule> getGetPolicyMethod() {
    io.grpc.MethodDescriptor<policy.Policy.PolicyRuleId, policy.Policy.PolicyRule> getGetPolicyMethod;
    if ((getGetPolicyMethod = PolicyServiceGrpc.getGetPolicyMethod) == null) {
      synchronized (PolicyServiceGrpc.class) {
        if ((getGetPolicyMethod = PolicyServiceGrpc.getGetPolicyMethod) == null) {
          PolicyServiceGrpc.getGetPolicyMethod = getGetPolicyMethod =
              io.grpc.MethodDescriptor.<policy.Policy.PolicyRuleId, policy.Policy.PolicyRule>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetPolicy"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  policy.Policy.PolicyRuleId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  policy.Policy.PolicyRule.getDefaultInstance()))
              .setSchemaDescriptor(new PolicyServiceMethodDescriptorSupplier("GetPolicy"))
              .build();
        }
      }
    }
    return getGetPolicyMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.DeviceId,
      policy.Policy.PolicyRuleList> getGetPolicyByDeviceIdMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetPolicyByDeviceId",
      requestType = context.ContextOuterClass.DeviceId.class,
      responseType = policy.Policy.PolicyRuleList.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.DeviceId,
      policy.Policy.PolicyRuleList> getGetPolicyByDeviceIdMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.DeviceId, policy.Policy.PolicyRuleList> getGetPolicyByDeviceIdMethod;
    if ((getGetPolicyByDeviceIdMethod = PolicyServiceGrpc.getGetPolicyByDeviceIdMethod) == null) {
      synchronized (PolicyServiceGrpc.class) {
        if ((getGetPolicyByDeviceIdMethod = PolicyServiceGrpc.getGetPolicyByDeviceIdMethod) == null) {
          PolicyServiceGrpc.getGetPolicyByDeviceIdMethod = getGetPolicyByDeviceIdMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.DeviceId, policy.Policy.PolicyRuleList>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetPolicyByDeviceId"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.DeviceId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  policy.Policy.PolicyRuleList.getDefaultInstance()))
              .setSchemaDescriptor(new PolicyServiceMethodDescriptorSupplier("GetPolicyByDeviceId"))
              .build();
        }
      }
    }
    return getGetPolicyByDeviceIdMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.ServiceId,
      policy.Policy.PolicyRuleList> getGetPolicyByServiceIdMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetPolicyByServiceId",
      requestType = context.ContextOuterClass.ServiceId.class,
      responseType = policy.Policy.PolicyRuleList.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.ServiceId,
      policy.Policy.PolicyRuleList> getGetPolicyByServiceIdMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.ServiceId, policy.Policy.PolicyRuleList> getGetPolicyByServiceIdMethod;
    if ((getGetPolicyByServiceIdMethod = PolicyServiceGrpc.getGetPolicyByServiceIdMethod) == null) {
      synchronized (PolicyServiceGrpc.class) {
        if ((getGetPolicyByServiceIdMethod = PolicyServiceGrpc.getGetPolicyByServiceIdMethod) == null) {
          PolicyServiceGrpc.getGetPolicyByServiceIdMethod = getGetPolicyByServiceIdMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.ServiceId, policy.Policy.PolicyRuleList>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetPolicyByServiceId"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.ServiceId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  policy.Policy.PolicyRuleList.getDefaultInstance()))
              .setSchemaDescriptor(new PolicyServiceMethodDescriptorSupplier("GetPolicyByServiceId"))
              .build();
        }
      }
    }
    return getGetPolicyByServiceIdMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static PolicyServiceStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<PolicyServiceStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<PolicyServiceStub>() {
        @java.lang.Override
        public PolicyServiceStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new PolicyServiceStub(channel, callOptions);
        }
      };
    return PolicyServiceStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static PolicyServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<PolicyServiceBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<PolicyServiceBlockingStub>() {
        @java.lang.Override
        public PolicyServiceBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new PolicyServiceBlockingStub(channel, callOptions);
        }
      };
    return PolicyServiceBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static PolicyServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<PolicyServiceFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<PolicyServiceFutureStub>() {
        @java.lang.Override
        public PolicyServiceFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new PolicyServiceFutureStub(channel, callOptions);
        }
      };
    return PolicyServiceFutureStub.newStub(factory, channel);
  }

  /**
   */
  public static abstract class PolicyServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void policyAdd(policy.Policy.PolicyRule request,
        io.grpc.stub.StreamObserver<policy.Policy.PolicyRuleState> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getPolicyAddMethod(), responseObserver);
    }

    /**
     */
    public void policyUpdate(policy.Policy.PolicyRule request,
        io.grpc.stub.StreamObserver<policy.Policy.PolicyRuleState> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getPolicyUpdateMethod(), responseObserver);
    }

    /**
     */
    public void policyDelete(policy.Policy.PolicyRule request,
        io.grpc.stub.StreamObserver<policy.Policy.PolicyRuleState> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getPolicyDeleteMethod(), responseObserver);
    }

    /**
     */
    public void getPolicy(policy.Policy.PolicyRuleId request,
        io.grpc.stub.StreamObserver<policy.Policy.PolicyRule> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetPolicyMethod(), responseObserver);
    }

    /**
     */
    public void getPolicyByDeviceId(context.ContextOuterClass.DeviceId request,
        io.grpc.stub.StreamObserver<policy.Policy.PolicyRuleList> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetPolicyByDeviceIdMethod(), responseObserver);
    }

    /**
     */
    public void getPolicyByServiceId(context.ContextOuterClass.ServiceId request,
        io.grpc.stub.StreamObserver<policy.Policy.PolicyRuleList> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetPolicyByServiceIdMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getPolicyAddMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                policy.Policy.PolicyRule,
                policy.Policy.PolicyRuleState>(
                  this, METHODID_POLICY_ADD)))
          .addMethod(
            getPolicyUpdateMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                policy.Policy.PolicyRule,
                policy.Policy.PolicyRuleState>(
                  this, METHODID_POLICY_UPDATE)))
          .addMethod(
            getPolicyDeleteMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                policy.Policy.PolicyRule,
                policy.Policy.PolicyRuleState>(
                  this, METHODID_POLICY_DELETE)))
          .addMethod(
            getGetPolicyMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                policy.Policy.PolicyRuleId,
                policy.Policy.PolicyRule>(
                  this, METHODID_GET_POLICY)))
          .addMethod(
            getGetPolicyByDeviceIdMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.DeviceId,
                policy.Policy.PolicyRuleList>(
                  this, METHODID_GET_POLICY_BY_DEVICE_ID)))
          .addMethod(
            getGetPolicyByServiceIdMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.ServiceId,
                policy.Policy.PolicyRuleList>(
                  this, METHODID_GET_POLICY_BY_SERVICE_ID)))
          .build();
    }
  }

  /**
   */
  public static final class PolicyServiceStub extends io.grpc.stub.AbstractAsyncStub<PolicyServiceStub> {
    private PolicyServiceStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected PolicyServiceStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new PolicyServiceStub(channel, callOptions);
    }

    /**
     */
    public void policyAdd(policy.Policy.PolicyRule request,
        io.grpc.stub.StreamObserver<policy.Policy.PolicyRuleState> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getPolicyAddMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void policyUpdate(policy.Policy.PolicyRule request,
        io.grpc.stub.StreamObserver<policy.Policy.PolicyRuleState> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getPolicyUpdateMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void policyDelete(policy.Policy.PolicyRule request,
        io.grpc.stub.StreamObserver<policy.Policy.PolicyRuleState> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getPolicyDeleteMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getPolicy(policy.Policy.PolicyRuleId request,
        io.grpc.stub.StreamObserver<policy.Policy.PolicyRule> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetPolicyMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getPolicyByDeviceId(context.ContextOuterClass.DeviceId request,
        io.grpc.stub.StreamObserver<policy.Policy.PolicyRuleList> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetPolicyByDeviceIdMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getPolicyByServiceId(context.ContextOuterClass.ServiceId request,
        io.grpc.stub.StreamObserver<policy.Policy.PolicyRuleList> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetPolicyByServiceIdMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class PolicyServiceBlockingStub extends io.grpc.stub.AbstractBlockingStub<PolicyServiceBlockingStub> {
    private PolicyServiceBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected PolicyServiceBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new PolicyServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public policy.Policy.PolicyRuleState policyAdd(policy.Policy.PolicyRule request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getPolicyAddMethod(), getCallOptions(), request);
    }

    /**
     */
    public policy.Policy.PolicyRuleState policyUpdate(policy.Policy.PolicyRule request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getPolicyUpdateMethod(), getCallOptions(), request);
    }

    /**
     */
    public policy.Policy.PolicyRuleState policyDelete(policy.Policy.PolicyRule request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getPolicyDeleteMethod(), getCallOptions(), request);
    }

    /**
     */
    public policy.Policy.PolicyRule getPolicy(policy.Policy.PolicyRuleId request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetPolicyMethod(), getCallOptions(), request);
    }

    /**
     */
    public policy.Policy.PolicyRuleList getPolicyByDeviceId(context.ContextOuterClass.DeviceId request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetPolicyByDeviceIdMethod(), getCallOptions(), request);
    }

    /**
     */
    public policy.Policy.PolicyRuleList getPolicyByServiceId(context.ContextOuterClass.ServiceId request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetPolicyByServiceIdMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class PolicyServiceFutureStub extends io.grpc.stub.AbstractFutureStub<PolicyServiceFutureStub> {
    private PolicyServiceFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected PolicyServiceFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new PolicyServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<policy.Policy.PolicyRuleState> policyAdd(
        policy.Policy.PolicyRule request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getPolicyAddMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<policy.Policy.PolicyRuleState> policyUpdate(
        policy.Policy.PolicyRule request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getPolicyUpdateMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<policy.Policy.PolicyRuleState> policyDelete(
        policy.Policy.PolicyRule request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getPolicyDeleteMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<policy.Policy.PolicyRule> getPolicy(
        policy.Policy.PolicyRuleId request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetPolicyMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<policy.Policy.PolicyRuleList> getPolicyByDeviceId(
        context.ContextOuterClass.DeviceId request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetPolicyByDeviceIdMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<policy.Policy.PolicyRuleList> getPolicyByServiceId(
        context.ContextOuterClass.ServiceId request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetPolicyByServiceIdMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_POLICY_ADD = 0;
  private static final int METHODID_POLICY_UPDATE = 1;
  private static final int METHODID_POLICY_DELETE = 2;
  private static final int METHODID_GET_POLICY = 3;
  private static final int METHODID_GET_POLICY_BY_DEVICE_ID = 4;
  private static final int METHODID_GET_POLICY_BY_SERVICE_ID = 5;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final PolicyServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(PolicyServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_POLICY_ADD:
          serviceImpl.policyAdd((policy.Policy.PolicyRule) request,
              (io.grpc.stub.StreamObserver<policy.Policy.PolicyRuleState>) responseObserver);
          break;
        case METHODID_POLICY_UPDATE:
          serviceImpl.policyUpdate((policy.Policy.PolicyRule) request,
              (io.grpc.stub.StreamObserver<policy.Policy.PolicyRuleState>) responseObserver);
          break;
        case METHODID_POLICY_DELETE:
          serviceImpl.policyDelete((policy.Policy.PolicyRule) request,
              (io.grpc.stub.StreamObserver<policy.Policy.PolicyRuleState>) responseObserver);
          break;
        case METHODID_GET_POLICY:
          serviceImpl.getPolicy((policy.Policy.PolicyRuleId) request,
              (io.grpc.stub.StreamObserver<policy.Policy.PolicyRule>) responseObserver);
          break;
        case METHODID_GET_POLICY_BY_DEVICE_ID:
          serviceImpl.getPolicyByDeviceId((context.ContextOuterClass.DeviceId) request,
              (io.grpc.stub.StreamObserver<policy.Policy.PolicyRuleList>) responseObserver);
          break;
        case METHODID_GET_POLICY_BY_SERVICE_ID:
          serviceImpl.getPolicyByServiceId((context.ContextOuterClass.ServiceId) request,
              (io.grpc.stub.StreamObserver<policy.Policy.PolicyRuleList>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class PolicyServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    PolicyServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return policy.Policy.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("PolicyService");
    }
  }

  private static final class PolicyServiceFileDescriptorSupplier
      extends PolicyServiceBaseDescriptorSupplier {
    PolicyServiceFileDescriptorSupplier() {}
  }

  private static final class PolicyServiceMethodDescriptorSupplier
      extends PolicyServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    PolicyServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (PolicyServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new PolicyServiceFileDescriptorSupplier())
              .addMethod(getPolicyAddMethod())
              .addMethod(getPolicyUpdateMethod())
              .addMethod(getPolicyDeleteMethod())
              .addMethod(getGetPolicyMethod())
              .addMethod(getGetPolicyByDeviceIdMethod())
              .addMethod(getGetPolicyByServiceIdMethod())
              .build();
        }
      }
    }
    return result;
  }
}
