package policy;

import java.util.function.BiFunction;

import io.quarkus.grpc.runtime.MutinyClient;

@javax.annotation.Generated(
value = "by Mutiny Grpc generator",
comments = "Source: policy.proto")
public class PolicyServiceClient implements PolicyService, MutinyClient<MutinyPolicyServiceGrpc.MutinyPolicyServiceStub> {

    private final MutinyPolicyServiceGrpc.MutinyPolicyServiceStub stub;

    public PolicyServiceClient(String name, io.grpc.Channel channel, BiFunction<String, MutinyPolicyServiceGrpc.MutinyPolicyServiceStub, MutinyPolicyServiceGrpc.MutinyPolicyServiceStub> stubConfigurator) {
       this.stub = stubConfigurator.apply(name,MutinyPolicyServiceGrpc.newMutinyStub(channel));
    }

    @Override
    public MutinyPolicyServiceGrpc.MutinyPolicyServiceStub getStub() {
       return stub;
    }

    @Override
    public io.smallrye.mutiny.Uni<policy.Policy.PolicyRuleState> policyAdd(policy.Policy.PolicyRule request) {
       return stub.policyAdd(request);
    }
    @Override
    public io.smallrye.mutiny.Uni<policy.Policy.PolicyRuleState> policyUpdate(policy.Policy.PolicyRule request) {
       return stub.policyUpdate(request);
    }
    @Override
    public io.smallrye.mutiny.Uni<policy.Policy.PolicyRuleState> policyDelete(policy.Policy.PolicyRule request) {
       return stub.policyDelete(request);
    }
    @Override
    public io.smallrye.mutiny.Uni<policy.Policy.PolicyRule> getPolicy(policy.Policy.PolicyRuleId request) {
       return stub.getPolicy(request);
    }
    @Override
    public io.smallrye.mutiny.Uni<policy.Policy.PolicyRuleList> getPolicyByDeviceId(context.ContextOuterClass.DeviceId request) {
       return stub.getPolicyByDeviceId(request);
    }
    @Override
    public io.smallrye.mutiny.Uni<policy.Policy.PolicyRuleList> getPolicyByServiceId(context.ContextOuterClass.ServiceId request) {
       return stub.getPolicyByServiceId(request);
    }

}