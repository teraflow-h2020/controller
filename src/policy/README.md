# Policy Management TeraFlow OS service

The Policy Management service is tested on Ubuntu 20.04. Follow the instructions below to build, test, and run this service on your local environment.

## Compile code

`
./mvnw compile
`
## Execute unit tests

`
./mvnw test
`
## Run service

`
./mvnw quarkus:dev
`
