# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import grpc, logging
from common.tools.client.RetryDecorator import retry, delay_exponential
from l3_centralizedattackdetector.proto.l3_centralizedattackdetector_pb2_grpc import (
    L3CentralizedattackdetectorStub,
)
from l3_centralizedattackdetector.proto.l3_centralizedattackdetector_pb2 import (
    Empty,
    ModelInput,
    ModelOutput
)

LOGGER = logging.getLogger(__name__)
MAX_RETRIES = 15
DELAY_FUNCTION = delay_exponential(initial=0.01, increment=2.0, maximum=5.0)
RETRY_DECORATOR = retry(max_retries=MAX_RETRIES, delay_function=DELAY_FUNCTION, prepare_method_name='connect')

class l3_centralizedattackdetectorClient:
    def __init__(self, address, port):
        self.endpoint = "{}:{}".format(address, port)
        LOGGER.debug("Creating channel to {}...".format(self.endpoint))
        self.channel = None
        self.stub = None
        self.connect()
        LOGGER.debug("Channel created")

    def connect(self):
        self.channel = grpc.insecure_channel(self.endpoint)
        self.stub = L3CentralizedattackdetectorStub(self.channel)

    def close(self):
        if self.channel is not None:
            self.channel.close()
        self.channel = None
        self.stub = None

    @RETRY_DECORATOR
    def SendInput(self, request: ModelInput) -> Empty:
        LOGGER.debug('SendInput request: {}'.format(request))
        response = self.stub.SendInput(request)
        LOGGER.debug('SendInput result: {}'.format(response))
        return response
    
    @RETRY_DECORATOR
    def GetOutput(self, request: Empty) -> ModelOutput:
        LOGGER.debug('GetOutput request: {}'.format(request))
        response = self.stub.GetOutput(request)
        LOGGER.debug('GetOutput result: {}'.format(response))
        return response


