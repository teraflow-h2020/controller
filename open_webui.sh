# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# this script opens the webui

WEBUI_PROTO=`kubectl get service/webuiservice -n tf-dev -o jsonpath='{.spec.ports[0].name}'`
WEBUI_IP=`kubectl get service/webuiservice -n tf-dev -o jsonpath='{.spec.clusterIP}'`
WEBUI_PORT=`kubectl get service/webuiservice -n tf-dev -o jsonpath='{.spec.ports[0].port}'`
URL=${WEBUI_PROTO}://${WEBUI_IP}:${WEBUI_PORT}

echo Opening web UI on URL ${URL}

# curl -kL ${URL}

python3 -m webbrowser ${URL}