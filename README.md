# IMPORTANT - REPOSITORY HAS BEEN ARCHIVED - MOVED TO ETSI OSG TFS #

This repository is archived. All code base has been contributed to ETSI OSG TeraFlowSDN (TFS). Current repository is:
[https://labs.etsi.org/rep/tfs/controller](https://labs.etsi.org/rep/tfs/controller)

More info at: [tfs.etsi.org](tfs.etsi.org)
