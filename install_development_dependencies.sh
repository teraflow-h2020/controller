#!/bin/bash
# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# installing basic tools
pip install --upgrade pip setuptools wheel pip-tools pylint pytest pytest-benchmark coverage grpcio-tools

# creating an empty file
echo "" > requirements.in

#TODO: include here your component
COMPONENTS="compute context device service monitoring opticalcentralizedattackdetector opticalattackmitigator dbscanserving webui"

# compiling dependencies from all components
for component in $COMPONENTS
do
    echo "computing requirements for component $component"
    diff requirements.in src/$component/requirements.in | grep '^>' | sed 's/^>\ //' >> requirements.in
done

pip-compile --output-file=requirements.txt requirements.in
python -m pip install -r requirements.txt

# removing the temporary files
rm requirements.in
rm requirements.txt
